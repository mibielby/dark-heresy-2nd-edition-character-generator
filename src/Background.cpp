#include "Background.h"

AdminBackground::AdminBackground() :
		Background("Master of Paperwork", BACKGROUNDS::ADMIN,
				"Adeptus Administratum") {

#ifdef USE_CHOICE_TREE
	SkillChoiceTree* aSkillChoiceTree = new SkillChoiceTree(SKILLS::COMMERCE, "", SKILLS::MEDICAE, "");
	sStartingSkillChoices.emplace_back(aSkillChoiceTree);
#else
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::COMMERCE));
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::MEDICAE));
#endif
	sStartingSkills.emplace_back(SkillSelection(SKILLS::COMMON_LORE,"Adeptus Administratum"));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::LINGUISTICS, "High Gothic"));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::LOGIC));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::SCHOLASTIC_LORE));

#ifdef USE_CHOICE_TREE
	TalentChoiceTree* aTalentChoiceTree = new TalentChoiceTree(TALENTS::WEAPON_TRAINING, "Las", TALENTS::WEAPON_TRAINING, "SP");
	sStartingTalentChoices.emplace_back(aTalentChoiceTree);
#else
	sStartingTalentChoices.emplace_back(TalentSelection(TALENTS::WEAPON_TRAINING, "Las"));
	sStartingTalentChoices.emplace_back(TalentSelection(TALENTS::WEAPON_TRAINING, "SP"));
#endif

	sStartingEquipment.emplace_back("Imperial Robes");
	sStartingEquipment.emplace_back("Autoquill");
	sStartingEquipment.emplace_back("Chrono");
	sStartingEquipment.emplace_back("Dataslate");
	sStartingEquipment.emplace_back("Medi-kit");

#ifdef USE_CHOICE_TREE
	EquipmentChoiceTree* aEquipmentChoiceTree = new EquipmentChoiceTree("Laspistol", "Stub automatic");
	sStartingEquipmentChoices.emplace_back(aEquipmentChoiceTree);
#else
	sStartingEquipmentChoices.emplace_back("Laspistol");
	sStartingEquipmentChoices.emplace_back("Stub automatic");
#endif
	sBackgroundAptitudeChoices.emplace_back(APTITUDES::KNOWLEDGE);
	sBackgroundAptitudeChoices.emplace_back(APTITUDES::SOCIAL);

	sRecommendedRoles.emplace_back(ROLES::CHIRURGEON);
	sRecommendedRoles.emplace_back(ROLES::HIEROPHANT);
	sRecommendedRoles.emplace_back(ROLES::SAGE);
	sRecommendedRoles.emplace_back(ROLES::SEEKER);
}

ArbitesBackground::ArbitesBackground() :
		Background("The Face of the Law", BACKGROUNDS::ARBITES,
				"Adeptus Arbites") {

#ifdef USE_CHOICE_TREE
	SkillChoiceTree* aSkillChoiceTree = new SkillChoiceTree(SKILLS::INQUIRY, "", SKILLS::INTERROGATION, "");
	sStartingSkillChoices.emplace_back(aSkillChoiceTree);
#else
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::INQUIRY));
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::INTERROGATION));
#endif

	sStartingSkills.emplace_back(SkillSelection(SKILLS::INTIMIDATE));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::SCRUTINY));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::AWARENESS));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::COMMON_LORE,"Adeptus Arbites"));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::COMMON_LORE,"Underworld"));

#ifdef USE_CHOICE_TREE
	TalentChoiceTree* aTalentChoiceTree = new TalentChoiceTree(TALENTS::WEAPON_TRAINING, "Shock", TALENTS::WEAPON_TRAINING, "SP");
	sStartingTalentChoices.emplace_back(aTalentChoiceTree);
#else
	sStartingTalentChoices.emplace_back(SkillSelection(TALENTS::WEAPON_TRAINING, "Shock"));
	sStartingTalentChoices.emplace_back(SkillSelection(TALENTS::WEAPON_TRAINING, "SP"));
#endif

#ifdef USE_CHOICE_TREE
	EquipmentChoiceTree* aEquipmentChoiceTree = new EquipmentChoiceTree("Enforcer Light Carapace armour", "Carapace chestplate");
	sStartingEquipmentChoices.emplace_back(aEquipmentChoiceTree);

	EquipmentChoiceTree* anotherEquipmentChoiceTree = new EquipmentChoiceTree("Shotgun", "Shock maul");
	sStartingEquipmentChoices.emplace_back(anotherEquipmentChoiceTree);
#else
	//two sets
	sStartingEquipmentChoices.emplace_back(
			"Enforcer Light Carapace armour");
	sStartingEquipmentChoices.emplace_back("Carapace chestplate");

	sStartingEquipmentChoices.emplace_back("Shotgun");
	sStartingEquipmentChoices.emplace_back("Shock maul");
#endif

	sStartingEquipment.emplace_back("3 doses of stimm");
	sStartingEquipment.emplace_back("Manacles");
	sStartingEquipment.emplace_back("12 lho sticks");

	sBackgroundAptitudeChoices.emplace_back(APTITUDES::OFFENSE);
	sBackgroundAptitudeChoices.emplace_back(APTITUDES::DEFENSE);

	sRecommendedRoles.emplace_back(ROLES::ASSASSIN);
	sRecommendedRoles.emplace_back(ROLES::DESPERADO);
	sRecommendedRoles.emplace_back(ROLES::SEEKER);
	sRecommendedRoles.emplace_back(ROLES::WARRIOR);
}

TelepathicaBackground::TelepathicaBackground() :
		Background("The Constant Threat", BACKGROUNDS::TELEPATHICA,
				"Adeptus Astra Telepathica") {

	//Gains "Tested on Terra" if takes the Psyker elite advance

	sStartingSkills.emplace_back(SkillSelection(SKILLS::AWARENESS));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::COMMON_LORE,"Adeptus Astra Telepathica"));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::FORBIDDEN_LORE,"The Warp"));

#ifdef USE_CHOICE_TREE
	SkillChoiceTree* aSkillChoiceTree = new SkillChoiceTree(SKILLS::DECIEVE, "", SKILLS::INTERROGATION, "");
	sStartingSkillChoices.emplace_back(aSkillChoiceTree);

	SkillChoiceTree* anotherSkillChoiceTree = new SkillChoiceTree(SKILLS::PSYNISCIENCE, "", SKILLS::SCRUTINY, "");
	sStartingSkillChoices.emplace_back(anotherSkillChoiceTree);
#else

	//two sets
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::DECIEVE));
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::INTERROGATION));

	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::PSYNISCIENCE));
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::SCRUTINY));
#endif

#ifdef USE_CHOICE_TREE
	TalentChoiceTree* aTalentChoiceTree = new TalentChoiceTree(TALENTS::WEAPON_TRAINING, "Las", TALENTS::WEAPON_TRAINING, "Low-tech");
	sStartingTalentChoices.emplace_back(aTalentChoiceTree);
#else
	sStartingTalentChoices.emplace_back(TalentSelection(TALENTS::WEAPON_TRAINING,"Las"));
	sStartingTalentChoices.emplace_back(TalentSelection(TALENTS::WEAPON_TRAINING,"Low-tech"));
#endif

	sStartingEquipment.emplace_back("Laspistol");

#ifdef USE_CHOICE_TREE
	EquipmentChoiceTree* aEquipmentChoiceTree = new EquipmentChoiceTree("Staff", "Whip");
	sStartingEquipmentChoices.emplace_back(aEquipmentChoiceTree);

	EquipmentChoiceTree* anotherEquipmentChoiceTree = new EquipmentChoiceTree("Light flack cloak", "Flak vest");
	sStartingEquipmentChoices.emplace_back(anotherEquipmentChoiceTree);

	EquipmentChoiceTree* aThirdEquipmentChoiceTree = new EquipmentChoiceTree("Micro-bead", "Psy focus");
	sStartingEquipmentChoices.emplace_back(aThirdEquipmentChoiceTree);
#else
	//three sets
	sStartingEquipmentChoices.emplace_back("Staff");
	sStartingEquipmentChoices.emplace_back("Whip");

	sStartingEquipmentChoices.emplace_back("Light flack cloak");
	sStartingEquipmentChoices.emplace_back("flak vest");

	sStartingEquipmentChoices.emplace_back("Micro-bead");
	sStartingEquipmentChoices.emplace_back("Psy focus");
#endif

	sBackgroundAptitudeChoices.emplace_back(APTITUDES::DEFENSE);
	sBackgroundAptitudeChoices.emplace_back(APTITUDES::PSYKER);

	sRecommendedRoles.emplace_back(ROLES::CHIRURGEON);
	sRecommendedRoles.emplace_back(ROLES::MYSTIC);
	sRecommendedRoles.emplace_back(ROLES::SAGE);
	sRecommendedRoles.emplace_back(ROLES::SEEKER);
}

MechanicusBackground::MechanicusBackground() :
		Background("Replace the Weak Flesh", BACKGROUNDS::MECHANICUS, "Adeptus Mechanicus", "Mechanicus Implants") {


	sStartingSkills.emplace_back(SkillSelection(SKILLS::COMMON_LORE, "Adeptus Mechanicus"));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::LOGIC));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::SECURITY));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::TECH_USE));

#ifdef USE_CHOICE_TREE
	SkillChoiceTree* aSkillChoiceTree = new SkillChoiceTree(SKILLS::AWARENESS, "", SKILLS::OPERATE, "");
	sStartingSkillChoices.emplace_back(aSkillChoiceTree);
#else
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::AWARENESS));
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::OPERATE));
#endif

	sStartingTalents.emplace_back(TalentSelection(TALENTS::MECHADENDRITE_USE,"Utility"));
	sStartingTalents.emplace_back(TalentSelection(TALENTS::WEAPON_TRAINING,"SP"));

	sStartingEquipment.emplace_back("Imperial Robes");
	sStartingEquipment.emplace_back("2 vials of sacred ungents");

#ifdef USE_CHOICE_TREE
	EquipmentChoiceTree* aEquipmentChoiceTree = new EquipmentChoiceTree("Autogun", "Hand cannon");
	sStartingEquipmentChoices.emplace_back(aEquipmentChoiceTree);
#else
	sStartingEquipmentChoices.emplace_back("Autogun");
	sStartingEquipmentChoices.emplace_back("Hand cannon");
#endif

	sStartingEquipment.emplace_back("Monotask server-skull(utility)");
	sStartingEquipment.emplace_back("Optical Mechadendrite");

	sBackgroundAptitudeChoices.emplace_back(APTITUDES::KNOWLEDGE);
	sBackgroundAptitudeChoices.emplace_back(APTITUDES::TECH);

	sRecommendedRoles.emplace_back(ROLES::CHIRURGEON);
	sRecommendedRoles.emplace_back(ROLES::HIEROPHANT);
	sRecommendedRoles.emplace_back(ROLES::SAGE);
	sRecommendedRoles.emplace_back(ROLES::SEEKER);

	mStartingTrait = "Mechanicus Implants";
}

MinistorumBackground::MinistorumBackground() :
		Background("Faith is All", BACKGROUNDS::MINISTORUM, "Adeptus Ministorum") {

	sStartingSkills.emplace_back(SkillSelection(SKILLS::CHARM));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::COMMAND));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::COMMON_LORE,"Adeptus Ministorum"));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::LINGUISTICS,"High Gothic"));

#ifdef USE_CHOICE_TREE
	SkillChoiceTree* aSkillChoiceTree = new SkillChoiceTree(SKILLS::INQUIRY, "", SKILLS::SCRUTINY, "");
	sStartingSkillChoices.emplace_back(aSkillChoiceTree);
#else
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::INQUIRY));
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::SCRUTINY));
#endif

	sStartingTalents.emplace_back(TALENTS::WEAPON_TRAINING); // Flame OR (low-tech AND SP)

	sStartingEquipment.emplace_back("Backpack");
	sStartingEquipment.emplace_back("Glow-globe");
	sStartingEquipment.emplace_back("Monotask servo-skull (laud hailer)");

#ifdef USE_CHOICE_TREE
	std::vector<std::string> aVec1;
	aVec1.emplace_back("Hand flamer");
	std::vector<std::string> aVec2;
	aVec2.emplace_back("Warhammer");
	aVec2.emplace_back("Stub revolver");
	EquipmentChoiceTree* aEquipmentChoiceTree = new EquipmentChoiceTree(aVec1, aVec2);
	sStartingEquipmentChoices.emplace_back(aEquipmentChoiceTree);

	EquipmentChoiceTree* aNotherEquipmentChoiceTree = new EquipmentChoiceTree("Imperial Robes", "Flak vest");
	sStartingEquipmentChoices.emplace_back(aNotherEquipmentChoiceTree);
#else
	sStartingEquipmentChoices.emplace_back("Hand flamer");
	sStartingEquipmentChoices.emplace_back("Warhammer and stub revolver"); //choice of 1 or 2 items

	sStartingEquipmentChoices.emplace_back("Imperial Robes");
	sStartingEquipmentChoices.emplace_back("Flak vest");
#endif

	sBackgroundAptitudeChoices.emplace_back(APTITUDES::LEADERSHIP);
	sBackgroundAptitudeChoices.emplace_back(APTITUDES::SOCIAL);

	sRecommendedRoles.emplace_back(ROLES::CHIRURGEON);
	sRecommendedRoles.emplace_back(ROLES::HIEROPHANT);
	sRecommendedRoles.emplace_back(ROLES::SEEKER);
	sRecommendedRoles.emplace_back(ROLES::WARRIOR);
}

GuardBackground::GuardBackground() :
		Background("Hammer of the Emperor", BACKGROUNDS::GUARD, "Imperial Guard") {

	sStartingSkills.emplace_back(SkillSelection(SKILLS::ATHLETICS));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::COMMAND));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::COMMON_LORE,"Imperial Guard"));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::NAVIGATE,"Surface"));

#ifdef USE_CHOICE_TREE
	SkillChoiceTree* aSkillChoiceTree = new SkillChoiceTree(SKILLS::MEDICAE, "", SKILLS::OPERATE, "");
	sStartingSkillChoices.emplace_back(aSkillChoiceTree);
#else
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::MEDICAE));
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::OPERATE));
#endif

	sStartingTalents.emplace_back(TALENTS::WEAPON_TRAINING); //Las AND Low-tech

	sStartingEquipment.emplace_back("Combat vest");
	sStartingEquipment.emplace_back("Imperial Guard Flak armour");
	sStartingEquipment.emplace_back("Grapnel and line");
	sStartingEquipment.emplace_back("12 lho sticks");
	sStartingEquipment.emplace_back("Magnoculars");

#ifdef USE_CHOICE_TREE
	std::vector<std::string> aVec1;
	aVec1.emplace_back("Lasgun");
	std::vector<std::string> aVec2;
	aVec2.emplace_back("Laspistol");
	aVec2.emplace_back("Sword");
	EquipmentChoiceTree* aEquipmentChoiceTree = new EquipmentChoiceTree(aVec1, aVec2);
	sStartingEquipmentChoices.emplace_back(aEquipmentChoiceTree);
#else
	sStartingEquipmentChoices.emplace_back("Lasgun");
	sStartingEquipmentChoices.emplace_back("Laspistol and sword");//choice of 1 or 2 items
#endif
	sBackgroundAptitudeChoices.emplace_back(APTITUDES::LEADERSHIP);
	sBackgroundAptitudeChoices.emplace_back(APTITUDES::FIELDCRAFT);

	sRecommendedRoles.emplace_back(ROLES::ASSASSIN);
	sRecommendedRoles.emplace_back(ROLES::DESPERADO);
	sRecommendedRoles.emplace_back(ROLES::HIEROPHANT);
	sRecommendedRoles.emplace_back(ROLES::WARRIOR);
}

OutcastBackground::OutcastBackground() :
		Background("Never Quit", BACKGROUNDS::OUTCAST, "Outcast") {

	sStartingSkills.emplace_back(SkillSelection(SKILLS::COMMON_LORE, "Underworld"));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::DECIEVE));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::DODGE));
	sStartingSkills.emplace_back(SkillSelection(SKILLS::STEALTH));

#ifdef USE_CHOICE_TREE
	SkillChoiceTree* aSkillChoiceTree = new SkillChoiceTree(SKILLS::ACROBATICS, "", SKILLS::SLEIGHT_OF_HAND, "");
	sStartingSkillChoices.emplace_back(aSkillChoiceTree);
#else
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::ACROBATICS));
	sStartingSkillChoices.emplace_back(SkillSelection(SKILLS::SLEIGHT_OF_HAND));
#endif

	sStartingTalents.emplace_back(TALENTS::WEAPON_TRAINING); //Chain and (Las OR SP)

	sStartingEquipment.emplace_back("Chainsword");
	sStartingEquipment.emplace_back("Injector");

#ifdef USE_CHOICE_TREE
	EquipmentChoiceTree* aEquipmentChoiceTree = new EquipmentChoiceTree("Autopistol", "Las pistol");
	sStartingEquipmentChoices.emplace_back(aEquipmentChoiceTree);

	EquipmentChoiceTree* aNotherEquipmentChoiceTree = new EquipmentChoiceTree("Armoured Bodyglove", "Flak vest");
	sStartingEquipmentChoices.emplace_back(aNotherEquipmentChoiceTree);

	EquipmentChoiceTree* aThirdEquipmentChoiceTree = new EquipmentChoiceTree("2 doses of obscura", "2 doses of slaught");
	sStartingEquipmentChoices.emplace_back(aThirdEquipmentChoiceTree);
#else
	sStartingEquipmentChoices.emplace_back("Autopistol");
	sStartingEquipmentChoices.emplace_back("Las pistol");

	sStartingEquipmentChoices.emplace_back("Armoured Bodyglove");
	sStartingEquipmentChoices.emplace_back("Flak vest");

	sStartingEquipmentChoices.emplace_back("2 doses of obscura");
	sStartingEquipmentChoices.emplace_back("2 doses of slaught");
#endif

	sBackgroundAptitudeChoices.emplace_back(APTITUDES::FIELDCRAFT);
	sBackgroundAptitudeChoices.emplace_back(APTITUDES::SOCIAL);

	sRecommendedRoles.emplace_back(ROLES::ASSASSIN);
	sRecommendedRoles.emplace_back(ROLES::DESPERADO);
	sRecommendedRoles.emplace_back(ROLES::SEEKER);
}



