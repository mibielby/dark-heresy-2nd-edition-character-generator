#ifndef DH_2E_OUTPUT_PDF
#define DH_2E_OUTPUT_PDF

#include <stdio.h>
#include <string.h>

#include "Character.h"

class OutputPDF {

private:
	void outputPreamble(FILE * pFile);
	void outputCharacteristics(FILE * pFile);
	void outputExpFateInsanityCorruption(FILE * pFile);
	void outputAptitudes(FILE * pFile);
	void outputSkills(FILE * pFile);
	void outputTalentsTraits(FILE * pFile);
	void outputWeapons(FILE * pFile);
	void outputArmourDefence(FILE * pFile);
	void outputMovementFatigue(FILE * pFile);
	void outputGear(FILE * pFile);
	void outputPsychic(FILE * pFile);
	void outputSpecial(FILE * pFile);


	/** Skill outputting helper functions */
	bool checkForSkill(unsigned int pSkillID){
		std::map<unsigned int, Skill*>::iterator it = mChar->getSkills().find(pSkillID);
		if(it != mChar->getSkills().end()){
			return true;
		}
		return false;
	}

	void printSimpleSkill(FILE * pFile, unsigned int pSkillID, std::string pBoxNames[4]);
	void printCommonLore(FILE * pFile);
	void printForbiddenLore(FILE * pFile);
	void printLinguistics(FILE * pFile);
	void printNavigate(FILE * pFile);
	void printOperate(FILE * pFile);
	void printScholasticLore(FILE * pFile);
	void printTrade(FILE * pFile);



public:

	Character *mChar;

	OutputPDF(Character * pChar) : mChar (pChar) { /* EMPTY */ }

	void generate();

};

#endif
