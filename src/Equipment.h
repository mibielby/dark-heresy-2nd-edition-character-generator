#ifndef DH_2E_EQUIPMENT
#define DH_2E_EQUIPMENT

#include "Common.h"

#ifdef USE_BOOST_SERIAL
// include headers that implement a archive in simple text format
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/access.hpp>
#endif

using std::string;


class EquipmentChoiceNode {
private:
	std::vector<EquipmentChoiceNode*> mChildren;
	std::vector<std::string> mValue;


public:
	EquipmentChoiceNode(){};

	EquipmentChoiceNode(std::string pValue){
		mValue.emplace_back(pValue);
	};

	EquipmentChoiceNode(std::vector<std::string> pVec){
		for (std::string s : pVec){
			mValue.emplace_back(s);
		}
	};

	~EquipmentChoiceNode(){
		if (!isLeaf()){
			for (EquipmentChoiceNode* c : mChildren){
				delete c;
			}
		}
	};

	bool isLeaf() { return mChildren.empty(); }

	std::vector<std::string>* getValue() {
		if (!isLeaf()){
			return nullptr;
		} else {
			return &mValue;
		}
	}

	void addChild(EquipmentChoiceNode* pNode){
		mChildren.emplace_back(pNode);
	}

	std::vector<EquipmentChoiceNode*> getChildren() { return mChildren; }

};

/* Make a tree for choices, will generally be of form:
 * root - {A}
 *      - {B}
 * being a choice of A or B
 *
 * or
 * root - {A}
 *      - {B, C}
 *
 * being choice of {A} or {B&C}
 */
class EquipmentChoiceTree {
private:
	EquipmentChoiceNode* mRoot;

	EquipmentChoiceTree() : mRoot(nullptr) {}

public:

	/* Simplest and most common way of making a choice is between 2 Equipments */
	EquipmentChoiceTree(std::string pName1, std::string pName2){
		mRoot = new EquipmentChoiceNode();

		EquipmentChoiceNode* aLeaf1 = new EquipmentChoiceNode(pName1);
		EquipmentChoiceNode* aLeaf2 = new EquipmentChoiceNode(pName2);

		mRoot->addChild(aLeaf1);
		mRoot->addChild(aLeaf2);
	}

	EquipmentChoiceTree(std::vector<std::string> pVec1, std::vector<std::string> pVec2){
		mRoot = new EquipmentChoiceNode();

		EquipmentChoiceNode* aLeaf1 = new EquipmentChoiceNode(pVec1);
		EquipmentChoiceNode* aLeaf2 = new EquipmentChoiceNode(pVec2);

		mRoot->addChild(aLeaf1);
		mRoot->addChild(aLeaf2);
	}

	~EquipmentChoiceTree(){
		for (EquipmentChoiceNode* s : mRoot->getChildren()){
			delete s;
		}
	}

	EquipmentChoiceNode* getRoot() {return mRoot;}


	void makeChoices(std::vector<std::string>* pRetVec){
		//FIXME never put anything into pRetVec!

		for (EquipmentChoiceNode* c : mRoot->getChildren()){
			// children of root will never be leaf
			bool allLeaves = true;
			for (EquipmentChoiceNode* n : c->getChildren()){
				allLeaves = allLeaves & n->isLeaf();
			}

			if (allLeaves){
				// present the choice of Equipments
				printf("Please choose between the following Equipment sets:\n");
				int i=0;
				for (EquipmentChoiceNode* n : c->getChildren()){
					printf("%d:", i++);
					for (std::string s : *(n->getValue())){
						printf("%s ", s.c_str());
					} // end Equipment list in this choice option
					printf("\n");
				} // end choice

			} else {
				printf("Cannot cope with mixed leaves and internals!\n");
//FIXME				exit(1);
			}
		}

	}
};



#endif
