#ifndef DH_2E_CHARACTERISTIC
#define DH_2E_CHARACTERISTIC

#include "Common.h"

#ifdef USE_BOOST_SERIAL
// include headers that implement a archive in simple text format
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/access.hpp>
#endif

class Characteristics {

private:
	unsigned int mValue; // 0-100
	unsigned int mLevel; // 0-5;


#ifdef USE_BOOST_SERIAL
		friend class boost::serialization::access;
		// When the class Archive corresponds to an output archive, the
		// & operator is defined similar to <<.  Likewise, when the class Archive
		// is a type of input archive the & operator is defined similar to >>.
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & mValue;
			ar & mLevel;
		}
#endif

public:

	Characteristics(): mValue(0), mLevel(0) {};

	void init(unsigned int pValue) {
		mValue = 20 + pValue;
		mLevel = 0;
	}

	void setValue(unsigned int pValue) { mValue = pValue; }
	unsigned int getValue() { return mValue; }

	unsigned int getLevel() { return mLevel; }

	/** Returns true if lvl up succeeded */
	bool incLevel() {
		if (mLevel < 5){
			mLevel++;
			mValue += 5;
			return true;
		} else {
			printf("Cannot level up further\n");
			return false;
		}
	}
};


class CharacteristicsBlock {

private:

	// Zero aptitudes, one, two
	unsigned int sCosts[3][5] = {{500, 750, 1000, 1500, 2500},{250, 500, 750, 1000, 1500}, {100, 250, 500, 750, 1250}};

	Characteristics WS, BS, S, T, AG, INT, PER, WP, FEL;
	unsigned int IFL;


#ifdef USE_BOOST_SERIAL
		friend class boost::serialization::access;
		// When the class Archive corresponds to an output archive, the
		// & operator is defined similar to <<.  Likewise, when the class Archive
		// is a type of input archive the & operator is defined similar to >>.
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & sCosts;

			ar & WS;
			ar & BS;
			ar & S;
			ar & T;
			ar & AG;
			ar & INT;
			ar & PER;
			ar & WP;
			ar & FEL;
			ar & IFL;
		}
#endif

public:
	CharacteristicsBlock() : IFL(20) {}

	unsigned int getWS() {
		return WS.getValue();
	}

	void setWS(unsigned int pAmount) {
		WS.setValue(pAmount);
	}

	void initWS(unsigned int pAmount) {
		WS.init(pAmount);
	}

	unsigned int getBS() {
		return BS.getValue();
	}

	void setBS(unsigned int pAmount) {
		BS.setValue(pAmount);
	}

	void initBS(unsigned int pAmount) {
		BS.init(pAmount);
	}

	unsigned int getS() {
		return S.getValue();
	}

	void setS(unsigned int pAmount) {
		S.setValue(pAmount);
	}

	void initS(unsigned int pAmount) {
		S.init(pAmount);
	}

	unsigned int getT() {
		return T.getValue();
	}

	void setT(unsigned int pAmount) {
		T.setValue(pAmount);
	}

	void initT(unsigned int pAmount) {
		T.init(pAmount);
	}

	unsigned int getAG() {
		return AG.getValue();
	}

	void setAG(unsigned int pAmount) {
		AG.setValue(pAmount);
	}

	void initAG(unsigned int pAmount) {
		AG.init(pAmount);
	}

	unsigned int getINT() {
		return INT.getValue();
	}

	void setINT(unsigned int pAmount) {
		INT.setValue(pAmount);
	}

	void initINT(unsigned int pAmount) {
		INT.init(pAmount);
	}

	unsigned int getPER() {
		return PER.getValue();
	}

	void setPER(unsigned int pAmount) {
		PER.setValue(pAmount);
	}

	void initPER(unsigned int pAmount) {
		PER.init(pAmount);
	}

	unsigned int getWP() {
		return WP.getValue();
	}

	void setWP(unsigned int pAmount) {
		WP.setValue(pAmount);
	}

	void initWP(unsigned int pAmount) {
		WP.init(pAmount);
	}

	unsigned int getFEL() {
		return FEL.getValue();
	}

	void setFEL(unsigned int pAmount) {
		FEL.setValue(pAmount);
	}

	void initFEL(unsigned int pAmount) {
		FEL.init(pAmount);
	}

	unsigned int getIFL() {
		return IFL;
	}

	void setIFL(unsigned int pAmount) {
		IFL = pAmount;
	}

	void initIFL(unsigned int pAmount) {
		IFL = 20 + pAmount;
	}

	void incIFL(unsigned int pAmount){
		IFL += pAmount;
	}

	void decIFL(unsigned int pAmount){
		IFL -= pAmount;
	}

	void initCharacteristicByIndex(unsigned int pIndex, unsigned int pValue){

		switch(pIndex) {
			case 0:
				initWS(pValue);
				break;
			case 1:
				initBS(pValue);
				break;
			case 2:
				initS(pValue);
				break;
			case 3:
				initT(pValue);
				break;
			case 4:
				initAG(pValue);
				break;
			case 5:
				initINT(pValue);
				break;
			case 6:
				initPER(pValue);
				break;
			case 7:
				initWP(pValue);
				break;
			case 8:
				initFEL(pValue);
				break;
			case 9:
				initIFL(pValue);
				break;
			default:
				printf("Incorrect index");
		}
	}

	void setCharacteristicByIndex(unsigned int pIndex, unsigned int pValue){

			switch(pIndex) {
				case 0:
					setWS(pValue);
					break;
				case 1:
					setBS(pValue);
					break;
				case 2:
					setS(pValue);
					break;
				case 3:
					setT(pValue);
					break;
				case 4:
					setAG(pValue);
					break;
				case 5:
					setINT(pValue);
					break;
				case 6:
					setPER(pValue);
					break;
				case 7:
					setWP(pValue);
					break;
				case 8:
					setFEL(pValue);
					break;
				case 9:
					setIFL(pValue);
					break;
				default:
					printf("Incorrect index");
			}
		}

};

#endif
