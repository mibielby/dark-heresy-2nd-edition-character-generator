#ifndef DH_2E_SKILLS
#define DH_2E_SKILLS

#include "Common.h"
#include <vector>

using std::string;

#ifdef USE_BOOST_SERIAL
// include headers that implement a archive in simple text format
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/access.hpp>
#endif

struct SkillSelection {
private:
	unsigned int mSkillID;
	std::string mSubType;

	SkillSelection() : mSkillID(0), mSubType("Unset") { /* EMPTY */ }

public:
	SkillSelection(unsigned int pSkillID, std::string pSubType)
	: mSkillID(pSkillID), mSubType(pSubType) { /* EMPTY */ }

	SkillSelection(unsigned int pSkillID)
	: mSkillID(pSkillID), mSubType("") { /* EMPTY */ }

	const char* getSubType() { return mSubType.c_str(); }
	std::string getStringSubType() { return mSubType; }

	unsigned int getID() { return mSkillID;}
};

class Skill {
private:

	unsigned int mSkillID;

	unsigned int mAptitude1;
	unsigned int mAptitude2;

	unsigned int mLevel; //0-3

	bool mIsSpecialised;

	string mName;
	Skill() : mSkillID(0), mAptitude1(0), mAptitude2(0), mLevel(0), mIsSpecialised(false), mName("Unset"){}


#ifdef USE_BOOST_SERIAL
		friend class boost::serialization::access;
		// When the class Archive corresponds to an output archive, the
		// & operator is defined similar to <<.  Likewise, when the class Archive
		// is a type of input archive the & operator is defined similar to >>.
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & mSkillID;
			ar & mAptitude1;
			ar & mAptitude2;

			ar & mLevel;

			ar & mIsSpecialised;

			ar & mName;
		}
#endif

public:
	Skill(unsigned int pSkillID, unsigned int pAptitude1, unsigned int pAptitude2, bool pIsSpecialised, string pName) :
		mSkillID(pSkillID), mAptitude1(pAptitude1), mAptitude2(pAptitude2), mLevel(0), mIsSpecialised(pIsSpecialised),  mName(pName){}

	const char* getName() { return mName.c_str(); }

	unsigned int getAptitude1() { return mAptitude1; }
	unsigned int getAptitude2() { return mAptitude2; }

	// TODO refactor to take aptitude list and search for number of matches?
	unsigned int getUpgradeCost(unsigned int pNumAptitudes){
		if (mLevel == 4){
			return 0;
		}
		return 300*(mLevel+1)*((3-pNumAptitudes)/3);
	}

	bool isComplex(){
		return mIsSpecialised;
	}

	const char* getLevelString(){
		string lvlString = "";
		if (mLevel == 1){
			lvlString = "+10";
		} else if (mLevel == 2){
			lvlString = "+20";
		} if (mLevel == 3){
			lvlString = "+30";
		} else {
			lvlString = "";
		}

		return lvlString.c_str();
	}

	void incLevel(){ mLevel++; }
	void decLevel(){ mLevel--; }

	unsigned int getSkillID(){return mSkillID;}
	unsigned int getLevel(){return mLevel;}
};

class ComplexSkill : public Skill {

private:
	string mSubType;

public:
	ComplexSkill(unsigned int pSkillID, unsigned int pAptitude1, unsigned int pAptitude2, string pName) :
		Skill(pSkillID, pAptitude1, pAptitude2,  true, pName) {}

	void setSubType(string pSubType) { mSubType = pSubType;}

	const char* getSubType() { return mSubType.c_str(); }
	string getStringSubType() { return mSubType; }
};

class BasicSkill : public Skill {
public:
	BasicSkill(unsigned int pSkillID, unsigned int pAptitude1, unsigned int pAptitude2, string pName) :
		Skill(pSkillID, pAptitude1, pAptitude2, false, pName) {}
};

/////////////////////////////////////////////////////////////////////////////////
// Basic Skills
/////////////////////////////////////////////////////////////////////////////////

class Acrobatics : public BasicSkill {
public:
	Acrobatics() : BasicSkill(SKILLS::ACROBATICS, APTITUDES::A_AG, APTITUDES::GENERAL, "Acrobatics") {}
};

class Athletics : public BasicSkill {
public:
	Athletics() : BasicSkill(SKILLS::ATHLETICS,APTITUDES::A_S, APTITUDES::GENERAL, "Athletics") {}
};

class Awareness : public BasicSkill {
public:
	Awareness() : BasicSkill(SKILLS::AWARENESS,APTITUDES::A_PER, APTITUDES::FIELDCRAFT, "Awareness") {}
};

class Charm : public BasicSkill {
public:
	Charm() : BasicSkill(SKILLS::CHARM,APTITUDES::A_FEL, APTITUDES::SOCIAL, "Charm") {}
};

class Command : public BasicSkill {
public:
	Command() : BasicSkill(SKILLS::COMMAND,APTITUDES::A_FEL, APTITUDES::LEADERSHIP, "Command") {}
};

class Commerce : public BasicSkill {
public:
	Commerce() : BasicSkill(SKILLS::COMMERCE, APTITUDES::A_INT, APTITUDES::KNOWLEDGE, "Commerce") {}
};

class Decieve : public BasicSkill {
public:
	Decieve() : BasicSkill(SKILLS::DECIEVE, APTITUDES::A_FEL, APTITUDES::SOCIAL, "Decieve") {}
};

class Dodge : public BasicSkill {
public:
	Dodge() : BasicSkill(SKILLS::DODGE, APTITUDES::A_AG, APTITUDES::DEFENSE, "Dodge") {}
};

class Inquiry : public BasicSkill {
public:
	Inquiry() : BasicSkill(SKILLS::INQUIRY, APTITUDES::A_FEL, APTITUDES::SOCIAL, "Inquiry") {}
};

class Interrogation : public BasicSkill {
public:
	Interrogation() : BasicSkill(SKILLS::INTERROGATION, APTITUDES::A_WP, APTITUDES::SOCIAL, "Interrogation") {}
};

class Intimidate : public BasicSkill {
public:
	Intimidate() : BasicSkill(SKILLS::INTIMIDATE, APTITUDES::A_S, APTITUDES::SOCIAL, "Intimidate") {}
};

class Logic : public BasicSkill {
public:
	Logic() : BasicSkill(SKILLS::LOGIC, APTITUDES::A_INT, APTITUDES::KNOWLEDGE, "Logic") {}
};

class Medicae : public BasicSkill {
public:
	Medicae() : BasicSkill(SKILLS::MEDICAE, APTITUDES::A_INT, APTITUDES::FIELDCRAFT, "Medicae") {}
};

class Parry : public BasicSkill {
public:
	Parry() : BasicSkill(SKILLS::PARRY, APTITUDES::A_WS, APTITUDES::DEFENSE, "Parry") {}
};

class Psyniscience : public BasicSkill {
public:
	Psyniscience() : BasicSkill(SKILLS::PSYNISCIENCE, APTITUDES::A_PER, APTITUDES::PSYKER, "Psyniscience") {}
};

class Scrutiny : public BasicSkill {
public:
	Scrutiny() : BasicSkill(SKILLS::SCRUTINY, APTITUDES::A_PER, APTITUDES::GENERAL, "Scrutiny") {}
};

class Security : public BasicSkill {
public:
	Security() : BasicSkill(SKILLS::SECURITY, APTITUDES::A_INT, APTITUDES::TECH, "Security") {}
};

class SleightOfHand : public BasicSkill {
public:
	SleightOfHand() : BasicSkill(SKILLS::SLEIGHT_OF_HAND, APTITUDES::A_AG, APTITUDES::KNOWLEDGE, "SleightOfHand") {}
};

class Stealth : public BasicSkill {
public:
	Stealth() : BasicSkill(SKILLS::STEALTH, APTITUDES::A_AG, APTITUDES::FIELDCRAFT, "Stealth") {}
};

class Survival : public BasicSkill {
public:
	Survival() : BasicSkill(SKILLS::SURVIVAL, APTITUDES::A_PER, APTITUDES::FIELDCRAFT, "Survival") {}
};

class TechUse : public BasicSkill {
public:
	TechUse() : BasicSkill(SKILLS::TECH_USE, APTITUDES::A_INT, APTITUDES::TECH, "TechUse") {}
};


/////////////////////////////////////////////////////////////////////////////////
// Complex Skills
/////////////////////////////////////////////////////////////////////////////////

class CommonLore : public ComplexSkill {
public:
//	enum CommonLoreSkills{
//		CommonLoreAdeptaSororitas,
//		CommonLoreAdeptusArbites,
//		CommonLoreAdeptusAstraTelepathica,
//		CommonLoreAdeptusMechanicus,
//		CommonLoreAdministratum,
//		CommonLoreAskellonSector,
//		CommonLoreChartistCaptains,
//		CommonLoreCollegiaTitanicus,
//		CommonLoreEcclesiarchy,
//		CommonLoreImperialCreed,
//		CommonLoreImperialGuard,
//		CommonLoreImperialNavy,
//		CommonLoreImperium,
//		CommonLoreNavigators,
//		CommonLorePlanetaryDefenceForces,
//		CommonLoreRogueTraders,
//		CommonLoreScholaProgenium,
//		CommonLoreTech,
//		CommonLoreUnderworld,
//		CommonLoreWar,
//	};
	CommonLore() : ComplexSkill(SKILLS::COMMON_LORE, APTITUDES::A_INT, APTITUDES::KNOWLEDGE, "Common Lore") {}
};

class ForbiddenLore : public ComplexSkill {
public:
	ForbiddenLore() : ComplexSkill(SKILLS::FORBIDDEN_LORE, APTITUDES::A_INT, APTITUDES::KNOWLEDGE, "Forbidden Lore") {}
};

class Linguistics : public ComplexSkill {
public:
	Linguistics() : ComplexSkill(SKILLS::LINGUISTICS, APTITUDES::A_INT, APTITUDES::GENERAL, "Linguistics") {}
};

class Navigate : public ComplexSkill {
public:
	Navigate() : ComplexSkill(SKILLS::NAVIGATE, APTITUDES::A_INT, APTITUDES::FIELDCRAFT, "Navigate") {}
};

class Operate : public ComplexSkill {
public:
	Operate() : ComplexSkill(SKILLS::OPERATE, APTITUDES::A_AG, APTITUDES::FIELDCRAFT, "Operate") {}
};

class ScholasticLore : public ComplexSkill {
public:
	ScholasticLore() : ComplexSkill(SKILLS::SCHOLASTIC_LORE, APTITUDES::A_INT, APTITUDES::KNOWLEDGE, "Scholastic Lore") {}
};

class Trade : public ComplexSkill {
public:
	Trade() : ComplexSkill(SKILLS::TRADE, APTITUDES::A_INT, APTITUDES::GENERAL, "Trade") {}
};

/////////////////////////////////////////////////////////////////////////////////
// Factory
/////////////////////////////////////////////////////////////////////////////////

class SkillFactory {
public:
	Skill *FactoryMethod(int type) {
		switch (type) {
			case SKILLS::ACROBATICS : {
				return new Acrobatics();
				break;
			}
			case SKILLS::ATHLETICS : {
				return new Athletics();
				break;
			}
			case SKILLS::AWARENESS : {
						return new Awareness();
						break;
					}
			case SKILLS::CHARM : {
						return new Charm();
						break;
					}
			case SKILLS::COMMAND : {
						return new Command();
						break;
					}
			case SKILLS::COMMERCE : {
						return new Commerce();
						break;
					}
			case SKILLS::COMMON_LORE : {
						return new CommonLore();
						break;
					}
			case SKILLS::DECIEVE : {
						return new Decieve();
						break;
					}
			case SKILLS::DODGE : {
						return new Dodge();
						break;
					}
			case SKILLS::FORBIDDEN_LORE : {
						return new ForbiddenLore();
						break;
					}
			case SKILLS::INQUIRY : {
						return new Inquiry();
						break;
					}
			case SKILLS::INTERROGATION : {
						return new Interrogation();
						break;
					}
			case SKILLS::INTIMIDATE : {
						return new Intimidate();
						break;
					}
			case SKILLS::LINGUISTICS : {
						return new Linguistics();
						break;
					}
			case SKILLS::LOGIC : {
						return new Logic();
						break;
					}
			case SKILLS::MEDICAE : {
						return new Medicae();
						break;
					}
			case SKILLS::NAVIGATE : {
						return new Navigate();
						break;
					}
			case SKILLS::OPERATE : {
						return new Operate();
						break;
					}
			case SKILLS::PARRY : {
						return new Parry();
						break;
					}
			case SKILLS::PSYNISCIENCE : {
						return new Psyniscience();
						break;
					}
			case SKILLS::SCHOLASTIC_LORE : {
						return new ScholasticLore();
						break;
					}
			case SKILLS::SCRUTINY : {
						return new Scrutiny();
						break;
					}
			case SKILLS::SECURITY : {
						return new Security();
						break;
					}
			case SKILLS::SLEIGHT_OF_HAND : {
						return new SleightOfHand();
						break;
					}
			case SKILLS::STEALTH : {
						return new Stealth();
						break;
					}
			case SKILLS::SURVIVAL : {
						return new Survival();
						break;
					}
			case SKILLS::TECH_USE : {
						return new TechUse();
						break;
					}
			case SKILLS::TRADE : {
						return new Trade();
						break;
					}
			default: {
				printf("ERRORR Skill unknown!\n");
				return nullptr;
				break;
			}
		}
	}
};



class SkillChoiceNode {
private:
	std::vector<SkillChoiceNode*> mChildren;
	std::vector<SkillSelection*> mValue;


public:
	SkillChoiceNode(){};

	SkillChoiceNode(SkillSelection* pValue){
		mValue.emplace_back(pValue);
	};

	~SkillChoiceNode(){
		if (isLeaf()){
			for (SkillSelection* v : mValue){
				delete v;
			}
		} else {
			for (SkillChoiceNode* c : mChildren){
				delete c;
			}
		}
	};

	bool isLeaf() { return mChildren.empty(); }

	std::vector<SkillSelection*>* getValue() {
		if (!isLeaf()){
			return nullptr;
		} else {
			return &mValue;
		}
	}

	void addChild(SkillChoiceNode* pNode){
		mChildren.emplace_back(pNode);
	}

	std::vector<SkillChoiceNode*> getChildren() { return mChildren; }

};

/* Make a tree for choices, will generally be of form:
 * root - {A}
 *      - {B}
 * being a choice of A or B
 *
 * or
 * root - {A}
 *      - {B, C}
 *
 * being choice of {A} or {B&C}
 */
class SkillChoiceTree {
private:
	SkillChoiceNode* mRoot;

	SkillChoiceTree() : mRoot(nullptr) {}
public:

	/* Simplest and most common way of making a choice is between 2 skills */
	SkillChoiceTree(unsigned int pSkillID1, std::string pSkillSubType1,
			unsigned int pSkillID2, std::string pSkillSubType2){
		mRoot = new SkillChoiceNode();

		SkillChoiceNode* aLeaf1 = new SkillChoiceNode(new SkillSelection(pSkillID1, pSkillSubType1));
		SkillChoiceNode* aLeaf2 = new SkillChoiceNode(new SkillSelection(pSkillID2, pSkillSubType2));

		mRoot->addChild(aLeaf1);
		mRoot->addChild(aLeaf2);

//		SkillChoiceNode* aChild = new SkillChoiceNode();
//		aChild->addChild(aLeaf1);
//		aChild->addChild(aLeaf2);
//		mRoot->addChild(aChild);
	}

	~SkillChoiceTree(){
		for (SkillChoiceNode* s : mRoot->getChildren()){
			delete s;
		}
	}

	SkillChoiceNode* getRoot() {return mRoot;}


	/**
	 * Make choices based on the structure of the tree.
	 * Is a pair to contain IDs and sub-types.
	 * Is a vector as some choices may return multiple results.
	 * @param pRetVec a vector to contain the choices made.
	 */
	void makeChoices(std::vector<std::pair<unsigned int, std::string> > pRetVec){

		//FIXME never populates pRetVec

		for (SkillChoiceNode* c : mRoot->getChildren()){
			// children of root will never be leaf
			bool allLeaves = true;
			for (SkillChoiceNode* n : c->getChildren()){
				allLeaves = allLeaves & n->isLeaf();
			}

			if (allLeaves){
				// present the choice of skills
				printf("Please choose between the following skill sets:\n");
				int i=0;
				for (SkillChoiceNode* n : c->getChildren()){
					printf("%d:", i);
					SkillFactory* aSkillFactory = new SkillFactory();
					for (SkillSelection* s : *(n->getValue())){
						Skill* skill = aSkillFactory->FactoryMethod(s->getID());

						if (skill->isComplex()) {
							// Check if there is a set subType, otherwise ask for it
							if (s->getStringSubType().compare("")== 0) {
								printf("Gained Specialised Skill %s\n",skill->getName());
								printf("%s (Pick one)", skill->getName());
							} else {
								printf("%s (%s)", skill->getName(), s->getSubType());
							}
						} else {
							printf("%s", skill->getName());
						} //end if complex

					} // end skill list in this choice option
					printf("\n");
					i++;
				} // end choice

			} else {
				printf("Cannot cope with mixed leaves and internals!\n");
//FIXME				exit(1);
			}
		}

	}
};

#endif


