#ifndef DH_2E_ROLE
#define DH_2E_ROLE

#include "Common.h"
#include "Talents.h"

#include <set>

#ifdef USE_BOOST_SERIAL
// include headers that implement a archive in simple text format
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/access.hpp>
#endif

class TalentChoiceTree;

class Role {

protected:
	std::string mRoleBonus;

	unsigned int mRoleID;
	std::string mName;

	// Provides a list of skills, some of which include a choice
	std::vector<unsigned int> sRoleAptitudesGiven;
	std::vector<unsigned int> sRoleAptitudesChoice; //only applies to assassin

#ifdef USE_CHOICE_TREE
	std::vector<TalentChoiceTree*> sRoleTalentChoice;
#else
	// Provides a choice of 2 talents, character may only pick one
	std::vector<Tale> sRoleTalentChoice;
#endif

#ifdef USE_BOOST_SERIAL
		friend class boost::serialization::access;
		// When the class Archive corresponds to an output archive, the
		// & operator is defined similar to <<.  Likewise, when the class Archive
		// is a type of input archive the & operator is defined similar to >>.
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & mRoleBonus;

			ar & mRoleID;
			ar & mName;

			// Provides a list of skills, some of which include a choice
			ar & sRoleAptitudesGiven;
			ar & sRoleAptitudesChoice; //only applies to assassin

			ar & sRoleTalentChoice;
		}
#endif

public:
	Role() :
			mRoleID(0), mName("Unset") { /* EMPTY */
	}

	Role(std::string pRoleBonus, unsigned int pRoleID, std::string pName) :
			mRoleBonus(pRoleBonus), mRoleID(pRoleID), mName(pName) { /* EMPTY */
	}

	const char* getName() {
		return mName.c_str();
	}

	std::string getStringName() {  return mName;	}

	unsigned int getID() {
		return mRoleID;
	}


	std::vector<unsigned int>* getRoleAptitudesGiven(){ return &sRoleAptitudesGiven;}
	std::vector<unsigned int>* getRoleAptitudesChoice(){ return &sRoleAptitudesChoice;}

	const char* getRoleBonus(){ return mRoleBonus.c_str();}
};

class Assassin: public Role {
public:
	Assassin();
};

class Chirurgeon: public Role {
public:
	Chirurgeon();
};

class Desperado: public Role {
public:
	Desperado();
};

class Hierophant: public Role {
public:
	Hierophant();
};

class Mystic: public Role {
public:
	Mystic();
};

class Sage: public Role {
public:
	Sage();
};

class Seeker: public Role {
public:
	Seeker();
};

class Warrior: public Role {
public:
	Warrior();
};

class RoleFactory {
public:
	Role *FactoryMethod(int type) {
		switch (type) {
		case ROLES::ASSASSIN: {
			return new Assassin();
			break;
		}
		case ROLES::CHIRURGEON: {
			return new Chirurgeon();
			break;
		}
		case ROLES::DESPERADO: {
			return new Desperado();
			break;
		}
		case ROLES::HIEROPHANT: {
			return new Hierophant();
			break;
		}
		case ROLES::MYSTIC: {
			return new Mystic();
			break;
		}
		case ROLES::SAGE: {
			return new Sage();
			break;
		}
		case ROLES::SEEKER: {
			return new Seeker();
			break;
		}
		case ROLES::WARRIOR: {
			return new Warrior();
			break;
		}
		default: {
			printf("ERRORR Role unknown!\n");
			return nullptr;
			break;
		}
		}
	}
};

#endif
