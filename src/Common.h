#ifndef DH_2E_COMMON
#define DH_2E_COMMON

#include <stdio.h>
#include <iostream>
#include <stdlib.h>

#define USE_BOOST_SERIAL

#define USE_CHOICE_TREE

enum CHARACTERISTICS {
	WS = 0, BS, S, T, AG, INT, PER, WP, FEL, IFL
};

enum APTITUDES {
	A_WS = 0,
	A_BS,
	A_S,
	A_T,
	A_AG,
	A_INT,
	A_PER,
	A_WP,
	A_FEL,
	GENERAL,
	OFFENSE,
	FINESSE,
	DEFENSE,
	PSYKER,
	TECH,
	KNOWLEDGE,
	LEADERSHIP,
	FIELDCRAFT,
	SOCIAL
};



enum BACKGROUNDS {
	ADMIN = 0, ARBITES, TELEPATHICA, MECHANICUS, MINISTORUM, GUARD, OUTCAST
};

enum HOMEWORLDS {
	FERRAL = 0, FORGE, HIGHBORN, HIVE, SHRINE, VOIDBORN
};

enum ROLES {
	ASSASSIN = 0,
	CHIRURGEON,
	DESPERADO,
	HIEROPHANT,
	MYSTIC,
	SAGE,
	SEEKER,
	WARRIOR
};

enum SKILLS {
	ACROBATICS = 0,
	ATHLETICS,
	AWARENESS,
	CHARM,
	COMMAND,
	COMMERCE,
	COMMON_LORE,
	DECIEVE,
	DODGE,
	FORBIDDEN_LORE,
	INQUIRY,
	INTERROGATION,
	INTIMIDATE,
	LINGUISTICS,
	LOGIC,
	MEDICAE,
	NAVIGATE,
	OPERATE,
	PARRY,
	PSYNISCIENCE,
	SCHOLASTIC_LORE,
	SCRUTINY,
	SECURITY,
	SLEIGHT_OF_HAND,
	STEALTH,
	SURVIVAL,
	TECH_USE,
	TRADE
};

enum TALENTS {
	// Tier 1
	AMBIDEXTROUS = 0,
	BLIND_FIGHTING,
	CATFALL,
	CLUES_FROM_THE_CROWDS,
	DIE_HARD,
	DISARM,
	DOUBLE_TEAM,
	ENEMY,
	FERRIC_SUMMONS,
	FRENZY,
	IRON_JAW, //10
	JADED,
	KEEN_INTUITION,
	LEAP_UP,
	NOWHERE_TO_HIDE,
	PEER,
	QUICK_DRAW,
	RAPID_RELOAD,
	RESISTANCE,
	SOUND_CONSTITUTION,
	TAKEDOWN, //20
	TECHNICAL_KNOCK,
	WARP_SENSE,
	WEAPON_TECH,
	WEAPON_TRAINING, //24
	// Tier 2
	ARMOUR_MONGER,
	BATTLE_RAGE,
	BULGING_BICEPS,
	COMBAT_MASTER,
	CONSTANT_VIGILANCE,
	CONTACT_NETWORK,
	COORDINATED_INTERROGATION,
	COUNTER_ATTACK,
	COVER_UP,
	DENY_THE_WITCH,
	DEVISTATING_ASSULT,
	DOUBLE_TAP,
	EXOTIC_WEAPON_TRAINING,
	FACE_IN_A_CROWD,
	HARD_TARGET,
	HARDY,
	HATRED,
	HIP_SHOOTING,
	INDEPENDANT_TARGETING,
	INESCAPABLE_ATTACK,
	KILLING_STRIKE,
	LUMINEN_SHOCK,
	MAGLEV_TRANSCENDENCE,
	MARKSMAN,
	MECHADENDRITE_USE,
	PRICISION_KILLER,
	PROSANGUINE,
	STRONG_MINDED,
	SWIFT_ATTACK,
	TWO_WEAPON_WIELDER,
	UNARMED_SPECIALIST,
	WARP_CONDUIT,
	WHIRLWIND_OF_DEATH,
	// Tier 3
	ADAMANTIUM_FAITH,
	ASSASIN_STRIKE,
	BASTION_OF_IRON_WILL,
	BLADEMASTER,
	CRUSHING_BLOW,
	DEATHDEALER,
	DELICATE_INTERROGATION,
	EYE_OF_VENGANCE,
	FAVOURED_BY_THE_WARP,
	FLASH_OF_INSIGHT,
	HALO_OF_COMMAND,
	HAMMER_BLOW,
	INFUSED_KNOWLEDGE,
	LIGHTNING_ATTACK,
	LUMINEN_BLAST,
	MASTERY,
	MIGHTY_SHOT,
	NEVER_DIE,
	PRETURNATURAL_SPEED,
	SPRINT,
	STEP_ASIDE,
	SUPERIOR_CHIRURGEON,
	TARGET_SELECTION,
	THUNDER_CHARGE,
	TRUE_GRIT,
	TWO_WEAPON_MASTER,
	WARP_LOCK
};

inline void printAptitude(unsigned int pIndex){
	switch (pIndex) {
	case 0:
        printf("Weapon Skill\n");
		break;
	case 1:
		printf("Ballistic Skill\n");
		break;
	case 2:
		printf("Strength\n");
		break;
	case 3:
		printf("Toughness\n");
		break;
	case 4:
		printf("Agility\n");
		break;
	case 5:
		printf("Intelligence\n");
		break;
	case 6:
		printf("Perception\n");
		break;
	case 7:
		printf("Willpower\n");
		break;
	case 8:
		printf("Fellowship\n");
		break;
	case 9:
		printf("General\n");
		break;
	case 10:
		printf("Offence\n");
		break;
	case 11:
		printf("Finesse\n");
		break;
	case 12:
		printf("Defence\n");
		break;
	case 13:
		printf("Psyker\n");
		break;
	case 14:
		printf("Tech\n");
		break;
	case 15:
		printf("Knowledge\n");
		break;
	case 16:
		printf("Leadership\n");
		break;
	case 17:
		printf("Fieldcraft\n");
		break;
	case 18:
		printf("Social\n");
		break;
	default :
		printf("Aptitude index out of range!");
	}
}

inline std::string getAptitudeByIndex(unsigned int i) {
	switch (i){
		case A_WS:
			return "Weapson Skill";
			break;
		case A_BS:
			return "Ballistic Skill";
			break;
		case A_S:
			return "Strength";
			break;
		case A_T:
			return "Toughness";
			break;
		case A_AG:
			return "Agility";
			break;
		case A_INT:
			return "Intelligence";
			break;
		case A_PER:
			return "Perception";
			break;
		case A_WP:
			return "Willpower";
			break;
		case A_FEL:
			return "Fellowship";
			break;
		case GENERAL:
			return "General";
			break;
		case OFFENSE:
			return "Offense";
			break;
		case FINESSE:
			return "Finess";
			break;
		case DEFENSE:
			return "Defense";
			break;
		case PSYKER:
			return "Psyker";
			break;
		case TECH:
			return "Tech";
			break;
		case KNOWLEDGE:
			return "Knowledge";
			break;
		case LEADERSHIP:
			return "Leadership";
			break;
		case FIELDCRAFT:
			return "Fieldcraft";
			break;
		case SOCIAL:
			return "Social";
			break;
		default :
			return "???";
	}
}

inline unsigned int acceptD5(std::string aReason){

	unsigned int aVal = 99;
	while (aVal > 5) {
		printf("Roll d5 %s:\n", aReason.c_str());
		std::string aStr = "";
		getline(std::cin, aStr);//FIXME sanitise input
		aVal = atoi(aStr.c_str());
	}

	return aVal;
}

inline unsigned int acceptD10(std::string aReason){

	unsigned int aVal = 99;
	while (aVal > 10) {
		printf("Roll d10 %s:\n", aReason.c_str());
		std::string aStr = "";
		getline(std::cin, aStr);//FIXME sanitise input
		aVal = atoi(aStr.c_str());
	}

	return aVal;
}

inline std::string acceptString(std::string aReason){

	std::string aStr = "";
	printf("Please enter %s:\n", aReason.c_str());
	getline(std::cin, aStr);//FIXME sanitise input
	return aStr;
}

inline unsigned int acceptInt(std::string aReason){

	std::string aStr = "";
	while (aStr.empty()){
		printf("Enter value %s:\n", aReason.c_str());
		getline(std::cin, aStr);//FIXME sanitise input
	}
	unsigned int aNum = atoi(aStr.c_str());
	return aNum;
}

#endif
