#include "Talents.h"

using std::string;


////////////////////////////////////////////////////////////////////////////
//// Tier One                                                             //
////////////////////////////////////////////////////////////////////////////


//requires Ag 30
bool Ambidextrous::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getAG() >=30){
		return true;
	}
	return false;
}

//requires Per 30
bool BlindFighting::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getPER() >=30){
		return true;
	}
	return false;
}


//requires Ag 30
bool Catfall::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getAG() >=30){
		return true;
	}
	return false;
}

//requires Fel 30
bool CluesFromTheCrowds::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getFEL() >=30){
		return true;
	}
	return false;
}

//requires WP 40
bool DieHard::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getWP() >= 40){
		return true;
	}
	return false;
}

//requires AG 30
bool Disarm::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getAG() >=30){
		return true;
	}
	return false;
}

//always available
bool DoubleTeam::isAvailable(Character* pChar){
	return true;
}

//always available
bool Enemy::isAvailable(Character* pChar){
	return true;
}

//requires Ferric Lure Implants, Mechanicus Implants
bool FerricSummons::isAvailable(Character* pChar){
	bool aHasFerricLure = false;
	bool aHasMechanicus = false;

	for (string s : pChar->getTraits()){
		if (s.compare("Ferric Lure Implants") == 0){
			aHasFerricLure = true;
		} else if (s.compare("Mechanicus Implants") == 0){
			aHasMechanicus = true;
		}
	}

	if (aHasFerricLure && aHasMechanicus){
		return true;
	}
	return false;
}

//always available
bool Frenzy::isAvailable(Character* pChar){
	return true;
}

//requires T 40
bool IronJaw::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getT() >= 40){
		return true;
	}
	return false;
}


//requires WP 40
bool Jaded::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getWP() >= 40){
		return true;
	}
	return false;
}

//requires Int 35
bool KeenIntuition::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getINT() >= 35){
		return true;
	}
	return false;
}

//requires AG 30
bool LeapUp::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getAG() >= 30){
		return true;
	}
	return false;
}

//requires Per 30
bool NowhereToHide::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getPER() >= 30){
		return true;
	}
	return false;
}

//requires Fel 30
bool Peer::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getFEL() >= 30){
		return true;
	}
	return false;
}

//always available
bool QuickDraw::isAvailable(Character* pChar){
	return true;
}

//always available
bool RapidReload::isAvailable(Character* pChar){
	return true;
}

//always available
bool Resistance::isAvailable(Character* pChar){
	return true;
}

//FIXME - need to update SoundConstition subtype when this is taken
//take multiple times, limit of up to twice TB
bool SoundConsitution::isAvailable(Character* pChar){
	if (pChar->getTalents().find(TALENTS::SOUND_CONSTITUTION) == pChar->getTalents().end()){
		return true;
	} else {
		ComplexTalent* sc = static_cast<ComplexTalent*> (pChar->getTalents()[TALENTS::SOUND_CONSTITUTION]);
		const char* aCount = sc->getSubType(); // Will be "1"/"2"/"3"/etc
		unsigned int aNum = atoi(aCount);
		if (aNum < (pChar->getCharacteristics()->getT() / 10)*2){
			return true;
		}
	}
	return false;
}

//always available
bool Takedown::isAvailable(Character* pChar){
	return true;
}

//requires Int 30
bool TechnicalKnock::isAvailable(Character* pChar){
	if (pChar->getCharacteristics()->getINT() >= 30){
		return true;
	}
	return false;
}

//requires Psy Rating, Psyniscience, Per 30
bool WarpSense::isAvailable(Character* pChar){

	bool aHasPsy = false;
	for (string s : pChar->getTraits()){
		if (s.compare(0,10,"Psy Rating") == 0){
			aHasPsy = true;
		}
	}
	if (!aHasPsy){
		return false;
	}
	if (pChar->getSkills().find(SKILLS::PSYNISCIENCE) == pChar->getSkills().end()){
		return false;
	}
	if (pChar->getCharacteristics()->getPER() >= 30){
		return true;
	}
	return false;
}

//requires Tech Use+10, Int 40
bool WeaponTech::isAvailable(Character* pChar){

	int aLvl = 0;

	if (pChar->getSkills().find(SKILLS::TECH_USE) == pChar->getSkills().end()){
		return false;
	} else {
		aLvl = pChar->getSkills()[SKILLS::TECH_USE]->getLevel();
	}

	if (pChar->getCharacteristics()->getINT() >=40 && aLvl >= 1){
		return true;
	}
	return false;
}

//always available
bool WeaponTraining::isAvailable(Character* pChar){
	return true;
}

////////////////////////////////////////////////////////////////////////////
//// Tier Two                                                             //
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
//// Tier Three                                                           //
////////////////////////////////////////////////////////////////////////////



