#ifndef DH_2E_BACKGROUND
#define DH_2E_BACKGROUND

#include "Common.h"
#include "Skills.h"
#include "Equipment.h"
#include "Talents.h"

#ifdef USE_BOOST_SERIAL
// include headers that implement a archive in simple text format
#include <boost/serialization/access.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#endif

class TalentSelection;
class TalentChoiceTree;

class Background {

protected:
	std::string mBackgroundBonus;

	unsigned int mBackgroundID;
	std::string mName;

	std::string mStartingTrait;

	std::vector<SkillSelection> sStartingSkills;
	std::vector<TalentSelection> sStartingTalents;
	std::vector<std::string> sStartingEquipment;

#ifdef USE_CHOICE_TREE
	std::vector<SkillChoiceTree*> sStartingSkillChoices;
	std::vector<EquipmentChoiceTree*>sStartingEquipmentChoices;
	std::vector<TalentChoiceTree*>sStartingTalentChoices;
#else
	std::vector<SkillSelection> sStartingSkillChoices;
	std::vector<std::string> sStartingEquipmentChoices;
	std::vector<TalentSelection>sStartingTalentChoices;
#endif

	std::vector<unsigned int> sRecommendedRoles;
	std::vector<unsigned int> sBackgroundAptitudeChoices;

#ifdef USE_BOOST_SERIAL
		friend class boost::serialization::access;
		// When the class Archive corresponds to an output archive, the
		// & operator is defined similar to <<.  Likewise, when the class Archive
		// is a type of input archive the & operator is defined similar to >>.
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & mBackgroundBonus;

			ar & mBackgroundID;
			ar & mName;

			ar & mStartingTrait;

			ar & sStartingSkills;
			ar & sStartingTalents;
			ar & sStartingEquipment;

			ar & sStartingSkillChoices;
			ar & sStartingEquipmentChoices;
			ar & sStartingTalentChoices;
			ar & sRecommendedRoles;
			ar & sBackgroundAptitudeChoices;
		}
#endif

public:
	Background() :
			mBackgroundID(0), mName("Unset"), mStartingTrait("") { /* EMPTY */
	}

	Background(std::string pBackgroundBonus, unsigned int pBackgroundID,
			std::string pName) :
			mBackgroundBonus(pBackgroundBonus), mBackgroundID(pBackgroundID), mName(
					pName), mStartingTrait("") {
	}

	Background(std::string pBackgroundBonus, unsigned int pBackgroundID,
			std::string pName, std::string pStartingTrait) :
			mBackgroundBonus(pBackgroundBonus), mBackgroundID(pBackgroundID), mName(
					pName), mStartingTrait(pStartingTrait) {
	}

	const char* getName() {
		return mName.c_str();
	}

	std::string getStringName() {  return mName; }

	unsigned int getID() {
		return mBackgroundID;
	}

	std::string getBackgroundBonus() { return mBackgroundBonus; }

	std::vector<SkillSelection> getStartingSkills() { return sStartingSkills; }
	std::vector<TalentSelection> getStartingTalents() { return sStartingTalents; }

	std::vector<std::string> getStartingEquipment() { return sStartingEquipment; }

#ifdef USE_CHOICE_TREE
	std::vector<SkillChoiceTree*> getStartingSkillChoices() { return sStartingSkillChoices; }
	std::vector<EquipmentChoiceTree*> getStartingEquipmentChoices() { return sStartingEquipmentChoices; }
	std::vector<TalentChoiceTree*> getStartingTalentChoices() { return sStartingTalentChoices; }
#else
	std::vector<SkillSelection> getStartingSkillChoices() { return sStartingSkillChoices; }
	std::vector<std::string> getStartingEquipmentChoices() { return sStartingEquipmentChoices; }
	std::vector<TalentSelection> getStartingTalentChoices() { return sStartingTalentChoices; }
#endif

	std::vector<unsigned int> getBackgroundAptitudeChoices () { return sBackgroundAptitudeChoices; }
	std::vector<unsigned int> getRecommendedRoles() { return sRecommendedRoles; }

	std::string getStartingTrait() { return mStartingTrait; }
};

class AdminBackground: public Background {
public:
	AdminBackground();
};

class ArbitesBackground: public Background {
public:
	ArbitesBackground();
};

class TelepathicaBackground: public Background {
public:
	TelepathicaBackground();
};

class MechanicusBackground: public Background {
public:
	MechanicusBackground();
};

class MinistorumBackground: public Background {
public:
	MinistorumBackground();
};

class GuardBackground: public Background {
public:
	GuardBackground();
};

class OutcastBackground: public Background {
public:
	OutcastBackground();
};

class BackgroundFactory {
public:
	Background *FactoryMethod(int type) {
		switch (type) {
		case BACKGROUNDS::ADMIN: {
			return new AdminBackground();
			break;
		}
		case BACKGROUNDS::ARBITES: {
			return new ArbitesBackground();
			break;
		}
		case BACKGROUNDS::GUARD: {
			return new GuardBackground();
			break;
		}
		case BACKGROUNDS::MECHANICUS: {
			return new MechanicusBackground();
			break;
		}
		case BACKGROUNDS::MINISTORUM: {
			return new MinistorumBackground();
			break;
		}
		case BACKGROUNDS::OUTCAST: {
			return new OutcastBackground();
			break;
		}
		case BACKGROUNDS::TELEPATHICA: {
			return new TelepathicaBackground();
			break;
		}
		default: {
			printf("ERRORR Background unknown!\n");
			return nullptr;
			break;
		}
		}
	}
};

#endif

