#ifndef DH_2E_Aptitude
#define DH_2E_Aptitude

#include "Common.h"

using std::string;

class AptitudeChoiceNode {
private:
	std::vector<AptitudeChoiceNode*> mChildren;
	std::vector<std::string> mValue;

public:
	AptitudeChoiceNode(){};

	AptitudeChoiceNode(std::string pValue){
		mValue.emplace_back(pValue);
	};

	~AptitudeChoiceNode(){
		if (!isLeaf()){
			for (AptitudeChoiceNode* c : mChildren){
				delete c;
			}
		}
	};

	bool isLeaf() { return mChildren.empty(); }

	std::vector<std::string>* getValue() {
		if (!isLeaf()){
			return nullptr;
		} else {
			return &mValue;
		}
	}

	void addChild(AptitudeChoiceNode* pNode){
		mChildren.emplace_back(pNode);
	}

	std::vector<AptitudeChoiceNode*> getChildren() { return mChildren; }

};

/* Make a tree for choices, will generally be of form:
 * root - {A}
 *      - {B}
 * being a choice of A or B
 *
 * or
 * root - {A}
 *      - {B, C}
 *
 * being choice of {A} or {B&C}
 */
class AptitudeChoiceTree {
private:
	AptitudeChoiceNode* mRoot;

public:

	/* Simplest and most common way of making a choice is between 2 Aptitudes */
	AptitudeChoiceTree(std::string pName1, std::string pName2){
		mRoot = new AptitudeChoiceNode();

		AptitudeChoiceNode* aLeaf1 = new AptitudeChoiceNode(pName1);
		AptitudeChoiceNode* aLeaf2 = new AptitudeChoiceNode(pName2);

		mRoot->addChild(aLeaf1);
		mRoot->addChild(aLeaf2);
	}

	~AptitudeChoiceTree(){
		for (AptitudeChoiceNode* s : mRoot->getChildren()){
			delete s;
		}
	}

	AptitudeChoiceNode* getRoot() {return mRoot;}


	void makeChoices(std::vector<std::string> pRetVec){
		//FIXME never populates pRetVec

		for (AptitudeChoiceNode* c : mRoot->getChildren()){
			// children of root will never be leaf
			bool allLeaves = true;
			for (AptitudeChoiceNode* n : c->getChildren()){
				allLeaves = allLeaves & n->isLeaf();
			}

			if (allLeaves){
				// present the choice of Aptitudes
				printf("Please choose between the following Aptitude sets:\n");
				int i=0;
				for (AptitudeChoiceNode* n : c->getChildren()){
					printf("%d:", i);
					for (std::string s : *(n->getValue())){
						printf("%s ", s.c_str());
					} // end Aptitude list in this choice option
					printf("\n");
					i++;
				} // end choice

			} else {
				printf("Cannot cope with mixed leaves and internals!\n");
				exit(1);
			}
		}

	}
};



#endif
