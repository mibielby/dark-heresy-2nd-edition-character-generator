#include "Role.h"


Assassin::Assassin() :
		Role("Sure Kill", ROLES::ASSASSIN, "Assassin") {

	sRoleAptitudesGiven.emplace_back(APTITUDES::A_AG);
	sRoleAptitudesGiven.emplace_back(APTITUDES::FIELDCRAFT);
	sRoleAptitudesGiven.emplace_back(APTITUDES::FINESSE);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_PER);

	// Pick one
	sRoleAptitudesChoice.emplace_back(APTITUDES::A_BS);
	sRoleAptitudesChoice.emplace_back(APTITUDES::A_WS);
#ifdef USE_CHOICE_TREE
	TalentChoiceTree* aTalentChoiceTree = new TalentChoiceTree(TALENTS::JADED, "", TALENTS::LEAP_UP, "");
	sRoleTalentChoice.emplace_back(aTalentChoiceTree);
#else
#endif
}

Chirurgeon::Chirurgeon() :
		Role("Dedicated Healer", ROLES::CHIRURGEON, "Chirurgeon") {

	sRoleAptitudesGiven.emplace_back(APTITUDES::FIELDCRAFT);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_INT);
	sRoleAptitudesGiven.emplace_back(APTITUDES::KNOWLEDGE);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_S);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_T);

#ifdef USE_CHOICE_TREE
	TalentChoiceTree* aTalentChoiceTree = new TalentChoiceTree(TALENTS::RESISTANCE, "", TALENTS::TAKEDOWN, "");
	sRoleTalentChoice.emplace_back(aTalentChoiceTree);
#else
	sRoleTalentChoice.emplace_back(TALENTS::RESISTANCE); // pick one
	sRoleTalentChoice.emplace_back(TALENTS::TAKEDOWN);
#endif

}

Desperado::Desperado() :
		Role("Move And Shoot", ROLES::DESPERADO, "Desperado") {

	sRoleAptitudesGiven.emplace_back(APTITUDES::A_AG);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_BS);
	sRoleAptitudesGiven.emplace_back(APTITUDES::DEFENSE);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_FEL);
	sRoleAptitudesGiven.emplace_back(APTITUDES::FINESSE);

#ifdef USE_CHOICE_TREE
	TalentChoiceTree* aTalentChoiceTree = new TalentChoiceTree(TALENTS::CATFALL, "", TALENTS::QUICK_DRAW, "");
	sRoleTalentChoice.emplace_back(aTalentChoiceTree);
#else
#endif

}


Hierophant::Hierophant() :
		Role("Sway the Masses", ROLES::HIEROPHANT, "Hierophant") {

	sRoleAptitudesGiven.emplace_back(APTITUDES::A_FEL);
	sRoleAptitudesGiven.emplace_back(APTITUDES::OFFENSE);
	sRoleAptitudesGiven.emplace_back(APTITUDES::SOCIAL);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_T);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_WP);

#ifdef USE_CHOICE_TREE
	TalentChoiceTree* aTalentChoiceTree = new TalentChoiceTree(TALENTS::DOUBLE_TEAM, "", TALENTS::HATRED, "");
	sRoleTalentChoice.emplace_back(aTalentChoiceTree);
#else
#endif

}

Mystic::Mystic() :
		Role("Stare into the Warp", ROLES::MYSTIC, "Mystic") {

	sRoleAptitudesGiven.emplace_back(APTITUDES::DEFENSE);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_INT);
	sRoleAptitudesGiven.emplace_back(APTITUDES::KNOWLEDGE);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_PER);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_WP);

#ifdef USE_CHOICE_TREE
	TalentChoiceTree* aTalentChoiceTree = new TalentChoiceTree(TALENTS::RESISTANCE, "Psychic Powers", TALENTS::WARP_SENSE, "");
	sRoleTalentChoice.emplace_back(aTalentChoiceTree);
#else
#endif

}

Sage::Sage() :
		Role("Quest for Knowledge", ROLES::SAGE, "Sage") {

	sRoleAptitudesGiven.emplace_back(APTITUDES::A_INT);
	sRoleAptitudesGiven.emplace_back(APTITUDES::KNOWLEDGE);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_PER);
	sRoleAptitudesGiven.emplace_back(APTITUDES::TECH);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_WP);

#ifdef USE_CHOICE_TREE
	TalentChoiceTree* aTalentChoiceTree = new TalentChoiceTree(TALENTS::AMBIDEXTROUS, "", TALENTS::CLUES_FROM_THE_CROWDS, "");
	sRoleTalentChoice.emplace_back(aTalentChoiceTree);
#else
#endif

}

Seeker::Seeker() :
		Role("Nothing Escapes My Sight", ROLES::SEEKER, "Seeker") {

	sRoleAptitudesGiven.emplace_back(APTITUDES::A_FEL);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_INT);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_PER);
	sRoleAptitudesGiven.emplace_back(APTITUDES::SOCIAL);
	sRoleAptitudesGiven.emplace_back(APTITUDES::TECH);

#ifdef USE_CHOICE_TREE
	TalentChoiceTree* aTalentChoiceTree = new TalentChoiceTree(TALENTS::KEEN_INTUITION, "", TALENTS::DISARM, "");
	sRoleTalentChoice.emplace_back(aTalentChoiceTree);
#else
#endif

}

Warrior::Warrior() :
		Role("Expert At Violence", ROLES::WARRIOR, "Warrior") {

	sRoleAptitudesGiven.emplace_back(APTITUDES::A_BS);
	sRoleAptitudesGiven.emplace_back(APTITUDES::DEFENSE);
	sRoleAptitudesGiven.emplace_back(APTITUDES::OFFENSE);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_S);
	sRoleAptitudesGiven.emplace_back(APTITUDES::A_WS);

#ifdef USE_CHOICE_TREE
	TalentChoiceTree* aTalentChoiceTree = new TalentChoiceTree(TALENTS::IRON_JAW, "", TALENTS::RAPID_RELOAD, "");
	sRoleTalentChoice.emplace_back(aTalentChoiceTree);
#else
#endif

}
