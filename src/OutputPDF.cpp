#include "OutputPDF.h"

/**
 * NB that checkboxes should be either 'Yes' or 'Off'
 *
 * TODO unknown field
 * fprintf( pFile, "Text25,\n");
 *
 */
void OutputPDF::outputPreamble(FILE * pFile) {
	// Intro
	fprintf(pFile, "Character Name,%s\n", mChar->getName());
	fprintf(pFile, "Home World,%s\n", mChar->getHomeworld()->getName());
	fprintf(pFile, "Background,%s\n",
			mChar->getBackground()->getName());
	fprintf(pFile, "Role,%s\n", mChar->getBackground()->getName());
//	fprintf(pFile, "Elite Advances,\n");
	fprintf(pFile, "Divination,%s\n",
			mChar->getBackgroundFluff()->mDivination.c_str());
	fprintf(pFile, "Notes 1,%s\n",
			mChar->getBackgroundFluff()->mNotes1.c_str());
	fprintf(pFile, "Notes 2,%s\n",
			mChar->getBackgroundFluff()->mNotes2.c_str());
	fprintf(pFile, "Player Name,%s\n", mChar->getPlayerName());
	fprintf(pFile, "Gender,%s\n", mChar->getBackgroundFluff()->mGender.c_str());
	fprintf(pFile, "Age,%d\n", mChar->getBackgroundFluff()->mAge);
	fprintf(pFile, "Build,%s\n", mChar->getBackgroundFluff()->mBuild.c_str());
	fprintf(pFile, "Complexion,%s\n",
			mChar->getBackgroundFluff()->mComplexion.c_str());
	fprintf(pFile, "Hair,%s\n", mChar->getBackgroundFluff()->mHair.c_str());
	fprintf(pFile, "Quirks,%s\n", mChar->getBackgroundFluff()->mQuirks.c_str());
	fprintf(pFile, "Superstitions,%s\n",
			mChar->getBackgroundFluff()->mSuperstitions.c_str());
	fprintf(pFile, "Mementos,%s\n",
			mChar->getBackgroundFluff()->mMementos.c_str());
//	fprintf(pFile, "Allies,\n");
//	fprintf(pFile, "Enemies,\n");
}

void OutputPDF::outputCharacteristics(FILE * pFile) {
	// Characteristics - Bullet points (Yes, Off)

//	// WS
//	fprintf(pFile, "31a,\n"); // WS top-right box
//	fprintf(pFile, "31,\n"); // WS top box
//	fprintf(pFile, "31b,\n");
//	fprintf(pFile, "31c,\n");
//	fprintf(pFile, "31d,\n");
//
//	// INT
//	fprintf(pFile, "32,\n");  // INT top-left
//	fprintf(pFile, "32a,\n"); // INT top-right
//	fprintf(pFile, "32b,\n");
//	fprintf(pFile, "32c,\n");
//	fprintf(pFile, "32d,\n");
//
//	// BS
//	fprintf(pFile, "33a,\n"); // BS top-left
//	fprintf(pFile, "33b,\n");
//	fprintf(pFile, "33c,\n");
//	fprintf(pFile, "33d,\n");
//	fprintf(pFile, "33e,\n"); // BS top-right
//
//	// PER
//	fprintf(pFile, "34a,\n"); // PER top-left
//	fprintf(pFile, "34b,\n"); // PER top-right
//	fprintf(pFile, "34c,\n");
//	fprintf(pFile, "34d,\n");
//	fprintf(pFile, "34e,\n");
//
//	// S
//	fprintf(pFile, "35a,\n"); // S top-left
//	fprintf(pFile, "35b,\n");
//	fprintf(pFile, "35c,\n");
//	fprintf(pFile, "35d,\n");
//	fprintf(pFile, "35e,\n"); // S top-right
//
//	// WP
//	fprintf(pFile, "36a,\n"); // WP top-left
//	fprintf(pFile, "36b,\n"); // WP top-right
//	fprintf(pFile, "36c,\n");
//	fprintf(pFile, "36d,\n");
//	fprintf(pFile, "36e,\n");
//
//	// T
//	fprintf(pFile, "37a,\n"); // W top-left
//	fprintf(pFile, "37b,\n"); // W top-right
//	fprintf(pFile, "37c,\n");
//	fprintf(pFile, "37d,\n");
//	fprintf(pFile, "37e,\n");
//
//	// FEL
//	fprintf(pFile, "38a,\n"); // FEL top-left
//	fprintf(pFile, "38b,\n"); // FEL top-right
//	fprintf(pFile, "38c,\n");
//	fprintf(pFile, "38d,\n");
//	fprintf(pFile, "38e,\n");
//
//	// AG
//	fprintf(pFile, "39a,\n"); // AG top-left
//	fprintf(pFile, "39b,\n");
//	fprintf(pFile, "39c,\n");
//	fprintf(pFile, "39d,\n");
//	fprintf(pFile, "39e,\n"); // AG top-right

// Characteristics - text : X1 is 10s, X2 is units
	fprintf(pFile, "WS1,%d\n", mChar->getCharacteristics()->getWS() / 10);
	fprintf(pFile, "WS2,%d\n", mChar->getCharacteristics()->getWS() % 10);

	fprintf(pFile, "BS1,%d\n", mChar->getCharacteristics()->getBS() / 10);
	fprintf(pFile, "BS2,%d\n", mChar->getCharacteristics()->getBS() % 10);

	fprintf(pFile, "S1,%d\n", mChar->getCharacteristics()->getS() / 10);
	fprintf(pFile, "S2,%d\n", mChar->getCharacteristics()->getS() % 10);

	fprintf(pFile, "T1,%d\n", mChar->getCharacteristics()->getT() / 10);
	fprintf(pFile, "T2,%d\n", mChar->getCharacteristics()->getT() % 10);

	fprintf(pFile, "A1,%d\n", mChar->getCharacteristics()->getAG() / 10);
	fprintf(pFile, "A2,%d\n", mChar->getCharacteristics()->getAG() % 10);

	fprintf(pFile, "I1,%d\n", mChar->getCharacteristics()->getINT() / 10);
	fprintf(pFile, "I2,%d\n", mChar->getCharacteristics()->getINT() % 10);

	fprintf(pFile, "P1,%d\n", mChar->getCharacteristics()->getPER() / 10);
	fprintf(pFile, "P2,%d\n", mChar->getCharacteristics()->getPER() % 10);

	fprintf(pFile, "WP1,%d\n", mChar->getCharacteristics()->getWP() / 10);
	fprintf(pFile, "WP2,%d\n", mChar->getCharacteristics()->getWP() % 10);

	fprintf(pFile, "Fel1,%d\n", mChar->getCharacteristics()->getFEL() / 10);
	fprintf(pFile, "Fel2,%d\n", mChar->getCharacteristics()->getFEL() % 10);

	fprintf(pFile, "Ifl1,%d\n", mChar->getCharacteristics()->getIFL() / 10);
	fprintf(pFile, "Ifl2,%d\n", mChar->getCharacteristics()->getIFL() % 10);
}

void OutputPDF::outputExpFateInsanityCorruption(FILE * pFile) {
	// Mental Disorders - Text
//	fprintf(pFile, "MD1,\n");
//	fprintf(pFile, "MD2,\n");
//	fprintf(pFile, "MD3,\n");
//	fprintf(pFile, "MD4,\n");
//	fprintf(pFile, "MD5,\n");
//	fprintf(pFile, "MD6,\n");
//	fprintf(pFile, "MD7,\n");
//
//	// Malignancies - Text
//	fprintf(pFile, "Mal1,\n");
//	fprintf(pFile, "Mal2,\n");
//	fprintf(pFile, "Mal3,\n");
//
//	// Mutations - Text
//	fprintf(pFile, "Mut1,\n");
//	fprintf(pFile, "Mut2,\n");
//	fprintf(pFile, "Mut3,\n");

// Insanity - Numbers - NO SPACES!
	fprintf(pFile, "Is1,%d\n", mChar->getInsanity() / 10);
	fprintf(pFile, "Is2,%d\n", mChar->getInsanity() % 10);

	// Corruption - Numbers - NO SPACES!
	fprintf(pFile, "C1,%d\n", mChar->getCorruption() / 10);
	fprintf(pFile, "C2,%d\n", mChar->getCorruption() % 10);

	// XP
	fprintf(pFile, "XP to spend,%d\n", mChar->getExpToSpend());
	fprintf(pFile, "XP Spent,%d\n",
			(mChar->getTotalExp() - mChar->getExpToSpend()));

	// FP
	fprintf(pFile, "Threshold,%d\n", mChar->getFate());
	fprintf(pFile, "Current,%d\n", mChar->getFate());
//
//	// Buttons under insanity/corruption - NO IDEA WHY! (Off, Yes)
//	fprintf(pFile, "Char1,\n"); // WS
//	fprintf(pFile, "Char2,\n"); // BS
//	fprintf(pFile, "Char3,\n"); // S
//	fprintf(pFile, "Char4,\n"); // T
//	fprintf(pFile, "Char5,\n"); // AG
//	fprintf(pFile, "Char6,\n"); // INT
//	fprintf(pFile, "Char7,\n"); // PER
//	fprintf(pFile, "Char8,\n"); // WP
//	fprintf(pFile, "Char9,\n"); // FEL
//	fprintf(pFile, "Char10,\n"); // IFL
}

void OutputPDF::outputAptitudes(FILE * pFile) {
	// Aptitudes - Text

	unsigned int i = 1;
	for (unsigned int a : *(mChar->getAptitudes())) {
		fprintf(pFile, "Apt%d,%s\n", i, getAptitudeByIndex(a).c_str());
		i++;
	}

}

void OutputPDF::printSimpleSkill(FILE * pFile, unsigned int pSkillID,
		std::string pBoxNames[4]) {
	if (checkForSkill(pSkillID)) {
		Skill* s = mChar->getSkills()[pSkillID];
		// Acrobatics
		fprintf(pFile, "Check Box%s,Yes\n", pBoxNames[0].c_str());
		if (s->getLevel() >= 1) {
			fprintf(pFile, "Check Box%s,Yes\n", pBoxNames[1].c_str());
		}
		if (s->getLevel() >= 2) {
			fprintf(pFile, "Check Box%s,Yes\n", pBoxNames[2].c_str());
		}
		if (s->getLevel() == 3) {
			fprintf(pFile, "Check Box%s,Yes\n", pBoxNames[3].c_str());
		}
	}
}

void OutputPDF::printCommonLore(FILE * pFile) {

	if (checkForSkill(SKILLS::COMMON_LORE)) {
		int skillCounter = 0;
		ComplexSkill* s =
				static_cast<ComplexSkill*>(mChar->getSkills()[SKILLS::COMMON_LORE]);
		switch (skillCounter) {
			case 0: {
				skillCounter++;
				fprintf(pFile, "Cl1,%s\n", s->getSubType());
				// Common Lore 1
				fprintf(pFile, "Check Box36,Yes\n");
				if (s->getLevel() >= 1) {
					fprintf(pFile, "Check Box36a,Yes\n");
				}
				if (s->getLevel() >= 2) {
					fprintf(pFile, "Check Box36b,Yes\n");
				}
				if (s->getLevel() == 3) {
					fprintf(pFile, "Check Box36d,Yes\n");
				}
				break;
			}
			case 1: {
				skillCounter++;
				fprintf(pFile, "Cl2,%s\n", s->getSubType());
				// Common Lore 2
				fprintf(pFile, "Check Box37,Yes\n");
				if (s->getLevel() >= 1) {
					fprintf(pFile, "Check Box37a,Yes\n");
				}
				if (s->getLevel() >= 2) {
					fprintf(pFile, "Check Box37b,Yes\n");
				}
				if (s->getLevel() == 3) {
					fprintf(pFile, "Check Box37d,Yes\n");
				}
				break;
			}
			case 2: {
				skillCounter++;
				fprintf(pFile, "Cl3,%s\n", s->getSubType());
				// Common Lore 3
				fprintf(pFile, "Check Box38,Yes\n");
				if (s->getLevel() >= 1) {
					fprintf(pFile, "Check Box38a,Yes\n");
				}
				if (s->getLevel() >= 2) {
					fprintf(pFile, "Check Box38b,Yes\n");
				}
				if (s->getLevel() == 3) {
					fprintf(pFile, "Check Box38d,Yes\n");
				}
				break;
			}
			case 3: {
				skillCounter++;
				fprintf(pFile, "Cl4,%s\n", s->getSubType());
				// Common Lore 4
				fprintf(pFile, "Check Box39,Yes\n");
				if (s->getLevel() >= 1) {
					fprintf(pFile, "Check Box39a,Yes\n");
				}
				if (s->getLevel() >= 2) {
					fprintf(pFile, "Check Box39b,Yes\n");
				}
				if (s->getLevel() == 3) {
					fprintf(pFile, "Check Box39d,Yes\n");
				}
				break;
			}
			default: {
				printf("Too many COMMON_LORE skills!\n");
			}
			//FIXME if more than 4 then panic!
			//TODO group skills of the same level together

		}
	}
}

void OutputPDF::printForbiddenLore(FILE * pFile) {
	if (checkForSkill(SKILLS::FORBIDDEN_LORE)) {
		int skillCounter = 0;
		ComplexSkill* s =
				static_cast<ComplexSkill*>(mChar->getSkills()[SKILLS::FORBIDDEN_LORE]);
		switch (skillCounter) {
			case 0: {
				skillCounter++;
				fprintf(pFile, "FL1,%s\n", s->getSubType());
				// Forbidden Lore 1
				fprintf(pFile, "Check Box42,Yes\n");
				if (s->getLevel() >= 1) {
					fprintf(pFile, "Check Box42a,Yes\n");
				}
				if (s->getLevel() >= 2) {
					fprintf(pFile, "Check Box43b,Yes\n");
				}
				if (s->getLevel() == 3) {
					fprintf(pFile, "Check Box43d,Yes\n");
				}
				break;
			}
			case 1: {
				skillCounter++;
				fprintf(pFile, "Fl2,%s\n", s->getSubType());
				// Forbidden Lore 2
				fprintf(pFile, "Check Box44,Yes\n");
				if (s->getLevel() >= 1) {
					fprintf(pFile, "Check Box44a,Yes\n");
				}
				if (s->getLevel() >= 2) {
					fprintf(pFile, "Check Box44b,Yes\n");
				}
				if (s->getLevel() == 3) {
					fprintf(pFile, "Check Box44d,Yes\n");
				}
				break;
			}
			case 2: {
				skillCounter++;
				fprintf(pFile, "Fl3,%s\n", s->getSubType());
				// Forbidden Lore 3
				fprintf(pFile, "Check Box45,Yes\n");
				if (s->getLevel() >= 1) {
					fprintf(pFile, "Check Box45a,Yes\n");
				}
				if (s->getLevel() >= 2) {
					fprintf(pFile, "Check Box45b,Yes\n");
				}
				if (s->getLevel() == 3) {
					fprintf(pFile, "Check Box45d,Yes\n");
				}
				break;
			}
			case 3: {
				skillCounter++;
				fprintf(pFile, "FL4,%s\n", s->getSubType());
				// Forbidden Lore 4
				fprintf(pFile, "Check Box46,Yes\n");
				if (s->getLevel() >= 1) {
					fprintf(pFile, "Check Box46a,Yes\n");
				}
				if (s->getLevel() >= 2) {
					fprintf(pFile, "Check Box46b,Yes\n");
				}
				if (s->getLevel() == 3) {
					fprintf(pFile, "Check Box46d,Yes\n");
				}
				break;
			}
			default: {
				printf("Too many FORBIDDEN_LORE choices!\n");
			}
			//FIXME if more than 4 then panic!
			//TODO group skills of the same level together
		}
	}
}

void OutputPDF::printLinguistics(FILE * pFile) {
	//FIXME COMPLEX!
	if (checkForSkill(SKILLS::LINGUISTICS)) {
		int skillCounter = 0;
		ComplexSkill* s = static_cast<ComplexSkill*>(mChar->getSkills()[SKILLS::LINGUISTICS]);
		switch (skillCounter) {
			case 0: {
				skillCounter++;
					fprintf(pFile, "LI1,%s\n", s->getSubType());
				//	// Linguistics 1
				//	fprintf(pFile, "Check Box50,Yes\n");
				//	fprintf(pFile, "Check Box50a,Yes\n");
				//	fprintf(pFile, "Check Box50b,Yes\n");
				//	fprintf(pFile, "Check Box50d,Yes\n");
				break;
			}
			case 1: {
				skillCounter++;
				//	// Linguistics 2
					fprintf(pFile, "LI2,%s\n", s->getSubType());
				//	fprintf(pFile, "Check Box51,Yes\n");
				//	fprintf(pFile, "Check Box51a,Yes\n");
				//	fprintf(pFile, "Check Box51b,Yes\n");
				//	fprintf(pFile, "Check Box51d,Yes\n");
				break;
			}
			default: {
				printf("Too many Linguistics skills!\n");
			}
			//FIXME if more than 2 then panic!
			//TODO group skills of the same level together
		}
	}
}

void OutputPDF::printNavigate(FILE * pFile) {
	//FIXME COMPLEX!
	if (checkForSkill(SKILLS::NAVIGATE)) {
		ComplexSkill* s = static_cast<ComplexSkill*>(mChar->getSkills()[SKILLS::NAVIGATE]);
		if (s->getStringSubType() == "Surface") {
			//	// Navigate(Surface)
			//	fprintf(pFile, "Check Box301,Yes\n");
			//	fprintf(pFile, "Check Box301a,Yes\n");
			//	fprintf(pFile, "Check Box301b,Yes\n");
			//	fprintf(pFile, "Check Box301d,Yes\n");
		} else if (s->getStringSubType() == "Stellar") {
			//	// Navigate(Stellar)
			//	fprintf(pFile, "Check Box302,Yes\n");
			//	fprintf(pFile, "Check Box302a,Yes\n");
			//	fprintf(pFile, "Check Box302b,Yes\n");
			//	fprintf(pFile, "Check Box302d,Yes\n");
		} else if (s->getStringSubType() == "Warp") {
			//	// Navigate(Warp)
			//	fprintf(pFile, "Check Box303,Yes\n");
			//	fprintf(pFile, "Check Box303a,Yes\n");
			//	fprintf(pFile, "Check Box303b,Yes\n");
			//	fprintf(pFile, "Check Box303d,Yes\n");
		} else {
			printf("Unknown Naviagte Skill: %s", s->getSubType());
		}
	}
}

void OutputPDF::printOperate(FILE * pFile) {
	//FIXME COMPLEX!
	if (checkForSkill(SKILLS::OPERATE)) {
		ComplexSkill* s = static_cast<ComplexSkill*>(mChar->getSkills()[SKILLS::OPERATE]);
		if (s->getStringSubType() == "Aeronautica") {
			//	// Operate (Aeronautica)
			//	fprintf(pFile, "Check Box304,Yes\n");
			//	fprintf(pFile, "Check Box304a,Yes\n");
			//	fprintf(pFile, "Check Box304b,Yes\n");
			//	fprintf(pFile, "Check Box304d,Yes\n");
		} else if (s->getStringSubType() == "Surface") {
			//	// Operate (Surface)
			//	fprintf(pFile, "Check Box305,Yes\n");
			//	fprintf(pFile, "Check Box305a,Yes\n");
			//	fprintf(pFile, "Check Box305b,Yes\n");
			//	fprintf(pFile, "Check Box305d,Yes\n");
		} else if (s->getStringSubType() == "Voidship") {
			//	// Operate (Voidship)
			//	fprintf(pFile, "Check Box306,Yes\n");
			//	fprintf(pFile, "Check Box306a,Yes\n");
			//	fprintf(pFile, "Check Box306b,Yes\n");
			//	fprintf(pFile, "Check Box306d,Yes\n");
		} else {
			printf("Unknown Operate Skill: %s", s->getSubType());
		}
	}
}

void OutputPDF::printScholasticLore(FILE * pFile) {
	//FIXME COMPLEX!

	if (checkForSkill(SKILLS::SCHOLASTIC_LORE)) {
		int skillCounter = 0;
		ComplexSkill* s = static_cast<ComplexSkill*>(mChar->getSkills()[SKILLS::SCHOLASTIC_LORE]);
		switch (skillCounter) {
			case 0: {
				skillCounter++;
				fprintf(pFile, "SL1,%s\n", s->getSubType());;
				//	// Scholastic Lore 1
				//	fprintf(pFile, "Check Box309,Yes\n");
				//	fprintf(pFile, "Check Box309a,Yes\n");
				//	fprintf(pFile, "Check Box309b,Yes\n");
				//	fprintf(pFile, "Check Box309d,Yes\n");
				break;
			}
			case 1: {
				skillCounter++;
					fprintf(pFile, "SL2,%s\n", s->getSubType());
				//	// Scholastic Lore 2
				//	fprintf(pFile, "Check Box3010,Yes\n");
				//	fprintf(pFile, "Check Box3010a,Yes\n");
				//	fprintf(pFile, "Check Box3010b,Yes\n");
				//	fprintf(pFile, "Check Box3010d,Yes\n");
				break;
			}
			case 2: {
				skillCounter++;
					fprintf(pFile, "SL3,%s\n", s->getSubType());
				//	// Scholastic Lore 3
				//	fprintf(pFile, "Check Box3011,Yes\n");
				//	fprintf(pFile, "Check Box3011a,Yes\n");
				//	fprintf(pFile, "Check Box3011b,Yes\n");
				//	fprintf(pFile, "Check Box3011d,Yes\n");
				break;
			}
			case 3: {
				skillCounter++;
					fprintf(pFile, "SL4,%s\n", s->getSubType());
				//	// Scholastic Lore 4
				//	fprintf(pFile, "Check Box3012,Yes\n");
				//	fprintf(pFile, "Check Box3012a,Yes\n");
				//	fprintf(pFile, "Check Box3012b,Yes\n");
				//	fprintf(pFile, "Check Box3012d,Yes\n");
				break;
			}
			case 4: {
				skillCounter++;
					fprintf(pFile, "SL5,%s\n", s->getSubType());;
				//	// Scholastic Lore 5
				//	fprintf(pFile, "Check Box3013,Yes\n");
				//	fprintf(pFile, "Check Box3013a,Yes\n");
				//	fprintf(pFile, "Check Box3013b,Yes\n");
				//	fprintf(pFile, "Check Box3013d,Yes\n");
				break;
			}
			default: {
				printf("Too many Scholastic Lore skills!\n");
			}
			//FIXME if more than 5 then panic!
			//TODO group skills of the same level together
		}
	}
}

void OutputPDF::printTrade(FILE * pFile) {
	//FIXME COMPLEX!

	if (checkForSkill(SKILLS::TRADE)) {
		int skillCounter = 0;
		ComplexSkill* s =
				static_cast<ComplexSkill*>(mChar->getSkills()[SKILLS::TRADE]);
		switch (skillCounter) {
			case 0: {
				skillCounter++;
					fprintf(pFile, "TR1,%s\n", s->getSubType());;
				//	// Trade 1
				//	fprintf(pFile, "Check Box3020,Yes\n");
				//	fprintf(pFile, "Check Box30a20,Yes\n");
				//	fprintf(pFile, "Check Box30b20,Yes\n");
				//	fprintf(pFile, "Check Box30d20,Yes\n");
				break;
			}
			case 1: {
				skillCounter++;
					fprintf(pFile, "TR2,%s\n", s->getSubType());
				//	// Trade 2
				//	fprintf(pFile, "Check Box3021,Yes\n");
				//	fprintf(pFile, "Check Box30a21,Yes\n");
				//	fprintf(pFile, "Check Box30b21,Yes\n");
				//	fprintf(pFile, "Check Box30d21,Yes\n");
				break;
			}
			case 2: {
				skillCounter++;
					fprintf(pFile, "TR3,%s\n", s->getSubType());
				//	// Trade 3
				//	fprintf(pFile, "Check Box3022,Yes\n");
				//	fprintf(pFile, "Check Box30a22,Yes\n");
				//	fprintf(pFile, "Check Box30b22,Yes\n");
				//	fprintf(pFile, "Check Box30d22,Yes\n");
				break;
			}
			default: {
				printf("Too many Trade Skills!\n");
			}
			//FIXME if more than 3 then panic!
			//TODO group skills of the same level together
		}
	}

}

void OutputPDF::outputSkills(FILE * pFile) {
	// Skills
	/**
	 * X is known
	 * Xa is +10
	 * Xb is +20
	 * -- Xc is missing APART from Athletics and Awareness
	 * Xd is +30
	 *
	 * Also breaks for forbidden lore 1
	 */

	// Acrobatics
	{
		std::string skillStrings[4] = { "30", "30a", "30b", "30d" };
		printSimpleSkill(pFile, SKILLS::ACROBATICS, skillStrings);
	}

	// Athletics
	{
		std::string skillStrings[4] = { "31", "31b", "31c", "31d" };
		printSimpleSkill(pFile, SKILLS::ATHLETICS, skillStrings);
	}

	// Awareness
	{
		std::string skillStrings[4] = { "32", "32a", "32b", "32c" };
		printSimpleSkill(pFile, SKILLS::AWARENESS, skillStrings);
	}

	// Charm
	{
		std::string skillStrings[4] = { "33", "33a", "33b", "33d" };
		printSimpleSkill(pFile, SKILLS::CHARM, skillStrings);
	}

	// Command
	{
		std::string skillStrings[4] = { "34", "34a", "34b", "34d" };
		printSimpleSkill(pFile, SKILLS::COMMAND, skillStrings);
	}

	// Commerce
	{
		std::string skillStrings[4] = { "35", "35a", "35b", "35d" };
		printSimpleSkill(pFile, SKILLS::COMMERCE, skillStrings);
	}

	// Common Lore Choices
	printCommonLore(pFile);

	// Deceive
	{
		std::string skillStrings[4] = { "40", "40a", "40b", "40d" };
		printSimpleSkill(pFile, SKILLS::DECIEVE, skillStrings);
	}

	// Dodge
	{
		std::string skillStrings[4] = { "41", "41a", "41b", "41d" };
		printSimpleSkill(pFile, SKILLS::DODGE, skillStrings);
	}

	// Forbidden Lore choices
	printForbiddenLore(pFile);

	// Inquiry
	{
		std::string skillStrings[4] = { "47", "47a", "47b", "47d" };
		printSimpleSkill(pFile, SKILLS::INQUIRY, skillStrings);
	}

	// Interrogation
	{
		std::string skillStrings[4] = { "48", "48a", "48b", "48d" };
		printSimpleSkill(pFile, SKILLS::INTERROGATION, skillStrings);
	}

	// Intimidate
	{
		std::string skillStrings[4] = { "49", "49a", "49b", "49d" };
		printSimpleSkill(pFile, SKILLS::INTIMIDATE, skillStrings);
	}

	// Linguistics choices
	printLinguistics(pFile);

	// Logic
	{
		std::string skillStrings[4] = { "52", "52a", "52b", "52d" };
		printSimpleSkill(pFile, SKILLS::LOGIC, skillStrings);
	}

	// Medicae
	{
		std::string skillStrings[4] = { "300", "300a", "300b", "300d" };
		printSimpleSkill(pFile, SKILLS::MEDICAE, skillStrings);
	}

	// Nagivate
	printNavigate(pFile);

	// Operate
	printOperate(pFile);

	// Parry
	{
		std::string skillStrings[4] = { "307", "307a", "307b", "307d" };
		printSimpleSkill(pFile, SKILLS::PARRY, skillStrings);
	}

	// Psyniscience
	{
		std::string skillStrings[4] = { "308", "308a", "308b", "308d" };
		printSimpleSkill(pFile, SKILLS::PSYNISCIENCE, skillStrings);
	}

	// Scholastic Lore choices
	printScholasticLore(pFile);

	// Scrutiny
	{
		std::string skillStrings[4] = { "3014", "3014a", "3014b", "3014d" };
		printSimpleSkill(pFile, SKILLS::SCRUTINY, skillStrings);
	}

	// Security
	{
		std::string skillStrings[4] = { "3015", "3015a", "3015b", "3015d" };
		printSimpleSkill(pFile, SKILLS::SECURITY, skillStrings);
	}

	// Sleight of Hand
	{
		std::string skillStrings[4] = { "3016", "3016a", "3016b", "3016d" };
		printSimpleSkill(pFile, SKILLS::SLEIGHT_OF_HAND, skillStrings);
	}

	// Stealth
	{
		std::string skillStrings[4] = { "3017", "3017a", "3017b", "3017d" };
		printSimpleSkill(pFile, SKILLS::STEALTH, skillStrings);
	}

	// Survival
	{
		std::string skillStrings[4] = { "3018", "3018a", "3018b", "3018d" };
		printSimpleSkill(pFile, SKILLS::SURVIVAL, skillStrings);
	}

	// Tech-Use
	{
		std::string skillStrings[4] = { "3019", "30a19", "30b19", "30d19" };
		printSimpleSkill(pFile, SKILLS::TECH_USE, skillStrings);
	}

	// Trade choices
	printTrade(pFile);

}

void OutputPDF::outputTalentsTraits(FILE * pFile) {
	// Talents and Traits - Text then number

	int i=0;
	for (std::pair<unsigned int, Talent*> p : mChar->getTalents()){
			fprintf(pFile, "T&T%d,%s\n", i, p.second->getName());
//			fprintf(pFile, "T&T%dp%d,%s\n", i, i);
			i++;
	}

	for (std::string s : mChar->getTraits()){
			fprintf(pFile, "T&T%d,%s\n", i, s.c_str());
//			fprintf(pFile, "T&T%dp%d,%s\n", i, i);
			i++;
	}

//	fprintf(pFile, "T&T1,\n");
//	fprintf(pFile, "T&T1p1,\n");

//FIXME
//	fprintf(pFile, "T&T2,\n");
//	fprintf(pFile, "T&T1p1a,\n");
//
//	fprintf(pFile, "T&T3,\n");
//	fprintf(pFile, "T&T1p2,\n");
//
//	fprintf(pFile, "T&T4,\n");
//	fprintf(pFile, "T&T1p3,\n");
//
//	fprintf(pFile, "T&T5,\n");
//	fprintf(pFile, "T&T1p4,\n");
//
//	fprintf(pFile, "T&T6,\n");
//	fprintf(pFile, "T&T1p5,\n");
//
//	fprintf(pFile, "T&T7,\n");
//	fprintf(pFile, "T&T1p6,\n");
//
//	fprintf(pFile, "T&T8,\n");
//	fprintf(pFile, "T&T1p7,\n");
//
//	fprintf(pFile, "T&T9,\n");
//	fprintf(pFile, "T&T1p8,\n");
//
//	fprintf(pFile, "T&T10,\n");
//	fprintf(pFile, "T&T1p9,\n");
//
//	fprintf(pFile, "T&T11,\n");
//	fprintf(pFile, "T&T1p10,\n");
//
//	fprintf(pFile, "T&T12,\n");
//	fprintf(pFile, "T&T1p11,\n");
//
//	fprintf(pFile, "T&T13,\n");
//	fprintf(pFile, "T&T1p12,\n");
//
//	fprintf(pFile, "T&T14,\n");
//	fprintf(pFile, "T&T1p13,\n");
}

void OutputPDF::outputWeapons(FILE * pFile) {
	// Weapons

//	fprintf(pFile, "WPName1,\n");
//	fprintf(pFile, "WPClass1,\n");
//	fprintf(pFile, "WPRange1,\n");
//	fprintf(pFile, "WPRoF1,\n");
//	fprintf(pFile, "WPDmg1,\n");
//	fprintf(pFile, "WPPen1,\n");
//	fprintf(pFile, "WPClip1,\n");
//	fprintf(pFile, "WPRld1,\n");
//	fprintf(pFile, "WPWt1,\n");
//	fprintf(pFile, "WPAvl1,\n");
//	fprintf(pFile, "WPSpecial1,\n");
//
//	fprintf(pFile, "WPName2,\n");
//	fprintf(pFile, "WPClass2,\n");
//	fprintf(pFile, "WPRange2,\n");
//	fprintf(pFile, "WPRoF2,\n");
//	fprintf(pFile, "WPDmg2,\n");
//	fprintf(pFile, "WPPen2,\n");
//	fprintf(pFile, "WPClip2,\n");
//	fprintf(pFile, "WPRld2,\n");
//	fprintf(pFile, "WPWt2,\n");
//	fprintf(pFile, "WPAvl2,\n");
//	fprintf(pFile, "WPSpecial2,\n");
//
//	fprintf(pFile, "WPName3,\n");
//	fprintf(pFile, "WPClass3,\n");
//	fprintf(pFile, "WPRange3,\n");
//	fprintf(pFile, "WPRoF3,\n");
//	fprintf(pFile, "WPDmg3,\n");
//	fprintf(pFile, "WPPen3,\n");
//	fprintf(pFile, "WPClip3,\n");
//	fprintf(pFile, "WPRld3,\n");
//	fprintf(pFile, "WPWt3,\n");
//	fprintf(pFile, "WPAvl3,\n");
//	fprintf(pFile, "WPSpecial3,\n");
//
//	fprintf(pFile, "WPName4,\n");
//	fprintf(pFile, "WPClass4,\n");
//	fprintf(pFile, "WPRange4,\n");
//	fprintf(pFile, "WPRoF4,\n");
//	fprintf(pFile, "WPDmg4,\n");
//	fprintf(pFile, "WPPen4,\n");
//	fprintf(pFile, "WPClip4,\n");
//	fprintf(pFile, "WPRld4,\n");
//	fprintf(pFile, "WPWt4,\n");
//	fprintf(pFile, "WPAvl4,\n");
//	fprintf(pFile, "WPSpecial4,\n");
//
//	fprintf(pFile, "WPName5,\n");
//	fprintf(pFile, "WPClass5,\n");
//	fprintf(pFile, "WPRange5,\n");
//	fprintf(pFile, "WPRoF5,\n");
//	fprintf(pFile, "WPDmg5,\n");
//	fprintf(pFile, "WPPen5,\n");
//	fprintf(pFile, "WPClip5,\n");
//	fprintf(pFile, "WPRld5,\n");
//	fprintf(pFile, "WPWt5,\n");
//	fprintf(pFile, "WPAvl5,\n");
//	fprintf(pFile, "WPSpecial5,\n");
//
//	fprintf(pFile, "WPName6,\n");
//	fprintf(pFile, "WPClass6,\n");
//	fprintf(pFile, "WPRange6,\n");
//	fprintf(pFile, "WPRoF6,\n");
//	fprintf(pFile, "WPDmg6,\n");
//	fprintf(pFile, "WPPen6,\n");
//	fprintf(pFile, "WPClip6,\n");
//	fprintf(pFile, "WPRld6,\n");
//	fprintf(pFile, "WPWt6,\n");
//	fprintf(pFile, "WPAvl6,\n");
//	fprintf(pFile, "WPSpecial6,\n");
}

void OutputPDF::outputArmourDefence(FILE * pFile) {

	// Armour - X1 is small, X2 is big

	/*
	 Custom Wounds by location:
	 Head = floor(CSW x 0.5) +1
	 Body = CSW
	 Limb = floor(CSW x 0.5) +2
	 */

	unsigned int headWounds = (mChar->getWounds() / 2.0) + 1;
	unsigned int bodyWounds = mChar->getWounds();
	unsigned int limbWounds = (mChar->getWounds() / 2.0) + 2;

	// Head
	fprintf(pFile, "H1,%d\n", mChar->getCharacteristics()->getT() / 10);// TODO add armour
	fprintf(pFile, "H2,%d\n", headWounds);

	// Right arm
	fprintf(pFile, "Ar1,%d\n", mChar->getCharacteristics()->getT() / 10);// TODO add armour
	fprintf(pFile, "Ar2,%d\n", limbWounds);

	// Left arm
	fprintf(pFile, "Al1,%d\n", mChar->getCharacteristics()->getT() / 10);// TODO add armour
	fprintf(pFile, "Al2,%d\n", limbWounds);

	// Body
	fprintf(pFile, "B1,%d\n", mChar->getCharacteristics()->getT() / 10);// TODO add armour
	fprintf(pFile, "B2,%d\n", bodyWounds);

	// Right leg
	fprintf(pFile, "Lr1,%d\n", mChar->getCharacteristics()->getT() / 10);// TODO add armour
	fprintf(pFile, "Lr2,%d\n", limbWounds);

	// Left leg
	fprintf(pFile, "Ll1,%d\n", mChar->getCharacteristics()->getT() / 10);// TODO add armour
	fprintf(pFile, "Ll2,%d\n", limbWounds);

	fprintf(pFile, "Wounds tlt,%d\n", mChar->getWounds());
	fprintf(pFile, "Wounds curr,%d\n", mChar->getWounds());
	fprintf(pFile, "Crit Dmg,0\n");

//	// Conditions
//
//	fprintf(pFile, "Cond1,\n");
//	fprintf(pFile, "Cond2,\n");
//	fprintf(pFile, "Cond3,\n");
//	fprintf(pFile, "Cond4,\n");
//	fprintf(pFile, "Cond5,\n");
//	fprintf(pFile, "Cond6,\n");
//	fprintf(pFile, "Cond7,\n");
//	fprintf(pFile, "Cond8,\n");
//	fprintf(pFile, "Cond9,\n");
//	fprintf(pFile, "Cond10,\n");
//	fprintf(pFile, "Cond11,\n");
//	fprintf(pFile, "Cond12,\n");
}

void OutputPDF::outputMovementFatigue(FILE * pFile) {

	unsigned int aBaseMove = mChar->getCharacteristics()->getAG() / 10;

	// Movement
	fprintf(pFile, "Half,%d\n", aBaseMove);
	fprintf(pFile, "Full,%d\n", aBaseMove * 2);
	fprintf(pFile, "Charge,%d\n", aBaseMove * 3);
	fprintf(pFile, "Run,%d\n", aBaseMove * 6);

	// Fatigue - TB+WPB
	fprintf(pFile, "Fatigue Thresh,%d\n",
			(mChar->getCharacteristics()->getT() / 10
					+ mChar->getCharacteristics()->getWP() / 10));
	fprintf(pFile, "Fatigue Current,0)\n");
}

void OutputPDF::outputGear(FILE * pFile) {

	unsigned int i = 1;
	for (std::string a : *(mChar->getEquipment())) {
		fprintf(pFile, "Gear%d,%s\n", i, a.c_str());
//		fprintf(pFile, "GearWt%d,%d\n", i, 0); //TODO
//		fprintf(pFile, "Gearp%d,%d\n", i, 0);
		i++;	// FIXME max 18
	}

//	// Weight
	fprintf(pFile, "Max Carry,%d\n",
			(mChar->getCharacteristics()->getT() / 10
					+ mChar->getCharacteristics()->getS() / 10));
//	fprintf(pFile, "Current Carry,%d\n", 0); //TODO

}

void OutputPDF::outputPsychic(FILE * pFile) {
	// Psy abilities

//	fprintf(pFile, "Psy Rating,\n");
//
//	fprintf(pFile, "Psy1,\n");
//	fprintf(pFile, "Psypage1,\n");
//
//	fprintf(pFile, "Psy2,\n");
//	fprintf(pFile, "Psypage2,\n");
//
//	fprintf(pFile, "Psy3,\n");
//	fprintf(pFile, "Psypage3,\n");
//
//	fprintf(pFile, "Psy4,\n");
//	fprintf(pFile, "Psypage4,\n");
//
//	fprintf(pFile, "Psy5,\n");
//	fprintf(pFile, "Psypage5,\n");
//
//	fprintf(pFile, "Psy6,\n");
//	fprintf(pFile, "Psypage6,\n");
//
//	fprintf(pFile, "Psy7,\n");
//	fprintf(pFile, "Psypage7,\n");
//
//	fprintf(pFile, "Psy8,\n");
//	fprintf(pFile, "Psypage8,\n");
}

void OutputPDF::outputSpecial(FILE * pFile) {

//	fprintf(pFile, "SA1,\n");
//	fprintf(pFile, "SA2,\n");
//	fprintf(pFile, "SA3,\n");
//	fprintf(pFile, "SA4,\n");
//	fprintf(pFile, "SA5,\n");
//	fprintf(pFile, "SA6,\n");
//	fprintf(pFile, "SA7,\n");
//	fprintf(pFile, "SA8,\n");
//	fprintf(pFile, "SA9,\n");
//	fprintf(pFile, "SA10,\n");
//	fprintf(pFile, "SA11,\n");
//	fprintf(pFile, "SA12,\n");

}

void OutputPDF::generate() {

	FILE * pFile;

	pFile = fopen("../PDF-editor/input.txt", "w");

	outputPreamble(pFile);
	outputCharacteristics(pFile);
	outputExpFateInsanityCorruption(pFile);
	outputAptitudes(pFile);
	outputSkills(pFile);
//	outputTalentsTraits(pFile);
//	outputWeapons(pFile);
	outputArmourDefence(pFile);
	outputMovementFatigue(pFile);
	outputGear(pFile);
//	outputPsychic(pFile);
//	outputSpecial(pFile);

	fclose(pFile);

	// Converts input.txt to FDF file data.fdf
	int result = system("cd ../PDF-editor; python main.py");

	if (result == 1) {
		printf("Failed to create FDF file.");
	} else {

//		char systemCmd[18 + 14 + mChar->getStringName().length() + 1];
		char systemCmd[18 + 14 + 50 + 1];

		strcpy(systemCmd, "cd ../PDF-editor; sh makePDF.sh ");
		strcat(systemCmd, mChar->getName());

		result = system(systemCmd);
		if (result == 1) {
			printf("Failed output PDF file.");
		}
	}

}

