#ifndef DH_2E_CHAR
#define DH_2E_CHAR

#include <string>
#include <vector>
#include <map>
#include <set>

#include <stdlib.h>

#include "Homeworld.h"
#include "Role.h"
#include "Background.h"
#include "Skills.h"
#include "Common.h"
#include "Characteristic.h"

#ifdef USE_BOOST_SERIAL
// include headers that implement a archive in simple text format
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/access.hpp>
#include <iostream>
#endif

using std::string;

class Background;
class Talent;
class Role;

class Character {

private:
	std::map<unsigned int, Homeworld*> *mHomeworlds;
	std::map<unsigned int, Background*> *mBackgrounds;
	std::map<unsigned int, Role*> *mRoles;


	Homeworld *mHomeworld;
	Background *mBackground;
	Role *mRole;

	string mHomeworldStr;
	string mBackgroundStr;
	string mRoleStr;

	unsigned int mTotalExp;
	unsigned int mExpToSpend;

	string mName;
	string mPlayerName;

	std::map<unsigned int, Skill*> mSkills;

	std::map<unsigned int, Talent*> mTalents;

	//FIXME - make std::map<unsigned int, Aptitude*>
	std::set<unsigned int> aptitudes;

	std::vector<string> equipment;

	std::vector<string> mTraits;

	CharacteristicsBlock mCharacteristics;

	unsigned int mFateThreshold;
	unsigned int mWounds;

	unsigned int mCorruption;
	unsigned int mInsanity;

	string mHomeworldBonus;
	string mBackgroundBonus;
	string mRoleBonus;


	struct BackgroundFluff {
		unsigned int mAge;

		string mDivination;
		string mGender;
		string mBuild;
		string mComplexion;
		string mHair;
		string mQuirks;
		string mSuperstitions;
		string mMementos;

		string mNotes1;
		string mNotes2;

		BackgroundFluff() :
			mAge(0), mDivination(""),mGender(""),mBuild(""),mComplexion(""),mHair(""),mQuirks(""), mSuperstitions(""),
			mMementos(""), mNotes1(""), mNotes2("")  { /* EMPTY */ }

#ifdef USE_BOOST_SERIAL
		friend class boost::serialization::access;
		// When the class Archive corresponds to an output archive, the
		// & operator is defined similar to <<.  Likewise, when the class Archive
		// is a type of input archive the & operator is defined similar to >>.
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{

			if (mDivination.empty()){

			}
			ar & mAge;

			ar & mDivination;
			ar & mGender;
			ar & mBuild;
			ar & mComplexion;
			ar & mHair;
			ar & mQuirks;
			ar & mSuperstitions;
			ar & mMementos;

			ar & mNotes1;
			ar & mNotes2;
		}
#endif

	} mBackgroundFluff;

	// Creation methods

	void setBackgroundFluff();
	void setCharacteristics();

	void setPlayerName();

	void setCharacterName();

	void setHomeworld();

	void setBackground();

	void setRole();

	void printCharacteristics();
	void printSkills();
	void printTalents();
	void printAptitudes();
	void printEquipment();
	void setBackgroundSkills();
	void setBackgroundTalents();
	void setBackgroundEquipment();


#ifdef USE_BOOST_SERIAL
	friend class boost::serialization::access;
	// When the class Archive corresponds to an output archive, the
	// & operator is defined similar to <<.  Likewise, when the class Archive
	// is a type of input archive the & operator is defined similar to >>.
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		//Not needed
//		ar & mHomeworlds;
//		ar & mBackgrounds;
//		ar & mRoles;

		//FIXME
//		ar & mHomeworldStr;
//		ar & mBackgroundStr; //breaks serialisation?
//		ar & mRoleStr;

		ar & mTotalExp;
		ar & mExpToSpend;

//		ar & mName;
//		ar & mPlayerName;

		//FIXME
//		ar & mSkills;
//		ar & mTalents;
//		ar & aptitudes;
//		ar & equipment;
//		ar & mTraits;
//		ar & mCharacteristics;

		ar & mFateThreshold;
		ar & mWounds;

		ar & mCorruption;
		ar & mInsanity;

		//FIXME
//		printf("Serialise mHomeworldBonus: %s\n", mHomeworldBonus.c_str());
//		ar & mHomeworldBonus;
//		printf("Serialise mBackgroundBonus: %s\n", mBackgroundBonus.c_str());
//		ar & mBackgroundBonus;
//		printf("Serialise mRoleBonus: %s\n", mRoleBonus.c_str());
//		ar & mRoleBonus;

//		ar & mBackgroundFluff;
	}

#endif

	// required by serialize
	Character() :
			mHomeworlds(nullptr), mBackgrounds(nullptr), mRoles(nullptr),
			mHomeworld(nullptr), mBackground(nullptr), mRole(nullptr), mHomeworldStr("Unset"), mBackgroundStr("Unset"), mRoleStr("Unset"), mTotalExp(500), mExpToSpend(500), mName("Unset"), mPlayerName("Unset"),
			mFateThreshold(0), mWounds(0), mCorruption(0), mInsanity(0), mHomeworldBonus("Unset"), mBackgroundBonus("Unset"), mRoleBonus("Unset")

	{ /* EMPTY */ }

public:



	Character(std::map<unsigned int, Homeworld*> *pHomeworlds, std::map<unsigned int, Background*> *pBackgrounds, std::map<unsigned int, Role*> *pRoles) :
		mHomeworlds(pHomeworlds), mBackgrounds(pBackgrounds), mRoles(pRoles),
		mHomeworld(nullptr), mBackground(nullptr), mRole(nullptr), mHomeworldStr("Unset"), mBackgroundStr("Unset"), mRoleStr("Unset"),  mTotalExp(500), mExpToSpend(500), mName("Unset"), mPlayerName("Unset"),
		mFateThreshold(0), mWounds(0), mCorruption(0), mInsanity(0), mHomeworldBonus("Unset"), mBackgroundBonus("Unset"), mRoleBonus("Unset")

	{ /* EMPTY */ }

	// Primary creation method
	void createCharacter();

	// Ongoing effects

	void addExp(unsigned int pAmount){ mTotalExp += pAmount; mExpToSpend += pAmount;	}

	void subExp(unsigned int pAmount){ mTotalExp -= pAmount; mExpToSpend -= pAmount;	}

	void addInfluence(unsigned int pAmmount){ mCharacteristics.incIFL(pAmmount); }

	void subInfluence(unsigned int pAmmount){ mCharacteristics.decIFL(pAmmount); }

	void printAllStats();

	const char* getName() { return mName.c_str(); }
	std::string getStringName() { return mName; }
	const char* getPlayerName() { return mPlayerName.c_str(); }
	Homeworld *getHomeworld() { return mHomeworld; }
	Background *getBackground() { return mBackground; }
	Role *getRole() { return mRole; }

	unsigned int getFate() { return mFateThreshold; }
	unsigned int getWounds() { return mWounds; }
	unsigned int getCorruption() { return mCorruption; }
	unsigned int getInsanity() { return mInsanity; }

	unsigned int getTotalExp() { return mTotalExp; }
	unsigned int getExpToSpend() { return mExpToSpend; }


	CharacteristicsBlock *getCharacteristics() { return &mCharacteristics; }
	std::set<unsigned int> *getAptitudes() { return &aptitudes; }

	std::vector<string> * getEquipment () { return &equipment; }

	BackgroundFluff *getBackgroundFluff(){ return &mBackgroundFluff; }

	std::map<unsigned int, Skill*> getSkills() { return mSkills;}

	std::map<unsigned int, Talent*> getTalents() { return mTalents;}

	std::vector<std::string> getTraits() { return mTraits;}

};

#endif
