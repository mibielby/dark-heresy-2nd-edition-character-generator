#ifndef DH_2E_TALENTS
#define DH_2E_TALENTS

#include "Common.h"
#include "Character.h"
#include "Skills.h"
#include <stdlib.h>

#ifdef USE_BOOST_SERIAL
// include headers that implement a archive in simple text format
#include <boost/serialization/access.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/string.hpp>
#endif

using std::string;

class Character;

struct TalentSelection {
private:
	unsigned int mTalentID;
	std::string mSubType;


	TalentSelection() : mTalentID(0), mSubType("Unset") { /* EMPTY */ }

public:
	TalentSelection(unsigned int pTalentID, std::string pSubType)
	: mTalentID(pTalentID), mSubType(pSubType) { /* EMPTY */ }

	TalentSelection(unsigned int pTalentID)
	: mTalentID(pTalentID), mSubType("") { /* EMPTY */ }

	const char* getSubType() { return mSubType.c_str(); }
	std::string getStringSubType() { return mSubType; }

	unsigned int getID() { return mTalentID;}
};

class Talent {
protected:

	// Zero aptitudes, one, two
	unsigned int sCosts[3][3] = {{600, 900, 1200},{300, 450, 600}, {200, 300, 400}};

	unsigned int mTalentID;
	unsigned int mAptitude1;
	unsigned int mAptitude2;

	bool mIsSpecialised;

	unsigned int mLevel; //0-2

	string mName;

	Talent() : mTalentID(0), mAptitude1(0), mAptitude2(0), mIsSpecialised(false),  mLevel(0), mName("Unset"){}

#ifdef USE_BOOST_SERIAL
		friend class boost::serialization::access;
		// When the class Archive corresponds to an output archive, the
		// & operator is defined similar to <<.  Likewise, when the class Archive
		// is a type of input archive the & operator is defined similar to >>.
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & sCosts;
			ar & mTalentID;
			ar & mAptitude1;
			ar & mAptitude2;

			ar & mIsSpecialised;

			ar & mLevel;

			ar & mName;
		}
#endif

public:

	Talent(unsigned int pTalentID, unsigned int pAptitude1, unsigned int pAptitude2 ,unsigned int pLevel, bool pIsSpecialised, string pName) :
			mTalentID(pTalentID), mAptitude1(pAptitude1), mAptitude2(pAptitude2), mIsSpecialised(pIsSpecialised), mLevel(pLevel),  mName(pName){}

	const char* getName() { return mName.c_str(); }

	unsigned int getAptitude1() { return mAptitude1; }
	unsigned int getAptitude2() { return mAptitude2; }

	// TODO refactor to take aptitude list and search for number of matches?
	unsigned int getCost(unsigned int pNumAptitudes){
		return sCosts[pNumAptitudes][mLevel];
	}

	bool isComplex(){
		return mIsSpecialised;
	}

	unsigned int getTalentID(){return mTalentID;}

	bool isAvailable(Character* pChar);

};

class ComplexTalent : public Talent {

private:
	string mSubType;

public:
	ComplexTalent(unsigned int pTalentID, unsigned int pAptitude1, unsigned int pAptitude2, unsigned int pLevel, string pName) :
		Talent(pTalentID, pAptitude1, pAptitude2,  pLevel, true, pName) {}

	void setSubType(string pSubType) { mSubType = pSubType;}

	const char* getSubType() { return mSubType.c_str(); }
};

class BasicTalent : public Talent {
public:
	BasicTalent(unsigned int pTalentID, unsigned int pAptitude1, unsigned int pAptitude2, unsigned int pLevel, string pName) :
		Talent(pTalentID, pAptitude1, pAptitude2, pLevel, false, pName) {}
};


//////////////////////////////////////////////////////////////////////////
// Tier One                                                             //
//////////////////////////////////////////////////////////////////////////

class Ambidextrous : public BasicTalent {
public:

	Ambidextrous() : BasicTalent(TALENTS::AMBIDEXTROUS, APTITUDES::A_WS, APTITUDES::A_BS, 0, "Ambidextrous") {}
	bool isAvailable(Character* pChar);
};

class BlindFighting : public BasicTalent {
public:
	BlindFighting() : BasicTalent(TALENTS::BLIND_FIGHTING, APTITUDES::A_PER, APTITUDES::FIELDCRAFT, 0, "Blind Fighting") {}
	bool isAvailable(Character* pChar);
};

class Catfall : public BasicTalent {
public:
	Catfall() : BasicTalent(TALENTS::CATFALL, APTITUDES::A_AG, APTITUDES::FIELDCRAFT, 0, "Catfall") {}
	bool isAvailable(Character* pChar);
};

class CluesFromTheCrowds : public BasicTalent {
public:
	CluesFromTheCrowds() : BasicTalent(TALENTS::CLUES_FROM_THE_CROWDS, APTITUDES::GENERAL, APTITUDES::SOCIAL, 0, "Clues From The Crowds") {}
	bool isAvailable(Character* pChar);
};

class DieHard : public BasicTalent {
public:
	DieHard() : BasicTalent(TALENTS::DIE_HARD, APTITUDES::A_WP, APTITUDES::DEFENSE, 0, "Die Hard") {}
	bool isAvailable(Character* pChar);
};

class Disarm : public BasicTalent {
public:
	Disarm() : BasicTalent(TALENTS::DISARM, APTITUDES::A_WS, APTITUDES::DEFENSE, 0, "Disarm") {}
	bool isAvailable(Character* pChar);
};

class DoubleTeam : public BasicTalent {
public:
	DoubleTeam() : BasicTalent(TALENTS::DOUBLE_TEAM, APTITUDES::GENERAL, APTITUDES::OFFENSE, 0, "Double Team") {}
	bool isAvailable(Character* pChar);
};

class Enemy : public ComplexTalent {
public:
	Enemy() : ComplexTalent(TALENTS::ENEMY, APTITUDES::GENERAL, APTITUDES::SOCIAL, 0, "Enemy") {}
	bool isAvailable(Character* pChar);
};

class FerricSummons : public BasicTalent {
public:
	FerricSummons() : BasicTalent(TALENTS::FERRIC_SUMMONS, APTITUDES::A_WP, APTITUDES::TECH, 0, "Ferric Summons") {}
	bool isAvailable(Character* pChar);
};

class Frenzy : public BasicTalent {
public:
	Frenzy() : BasicTalent(TALENTS::FRENZY, APTITUDES::A_S, APTITUDES::OFFENSE, 0, "Frenzy") {}
	bool isAvailable(Character* pChar);
};

class IronJaw : public BasicTalent {
public:
	IronJaw() : BasicTalent(TALENTS::IRON_JAW, APTITUDES::A_T, APTITUDES::DEFENSE, 0, "Iron Jaw") {}
	bool isAvailable(Character* pChar);
};

class Jaded : public BasicTalent {
public:
	Jaded() : BasicTalent(TALENTS::JADED, APTITUDES::A_WP, APTITUDES::DEFENSE, 0, "Jaded") {}
	bool isAvailable(Character* pChar);
};

class KeenIntuition : public BasicTalent {
public:
	KeenIntuition() : BasicTalent(TALENTS::KEEN_INTUITION, APTITUDES::A_PER, APTITUDES::SOCIAL, 0, "Keen Intuition") {}
	bool isAvailable(Character* pChar);
};

class LeapUp : public BasicTalent {
public:
	LeapUp() : BasicTalent(TALENTS::LEAP_UP, APTITUDES::A_AG, APTITUDES::GENERAL, 0, "Leap Up") {}
	bool isAvailable(Character* pChar);
};

class NowhereToHide : public BasicTalent {
public:
	NowhereToHide() : BasicTalent(TALENTS::NOWHERE_TO_HIDE, APTITUDES::A_FEL, APTITUDES::SOCIAL, 0, "Nowhere To Hide") {}
	bool isAvailable(Character* pChar);
};

class Peer : public ComplexTalent {
public:
	Peer() : ComplexTalent(TALENTS::PEER, APTITUDES::A_FEL, APTITUDES::SOCIAL, 0, "Peer") {}
	bool isAvailable(Character* pChar);
};

class QuickDraw : public BasicTalent {
public:
	QuickDraw() : BasicTalent(TALENTS::QUICK_DRAW, APTITUDES::A_AG, APTITUDES::FINESSE, 0, "Quick Draw") {}
	bool isAvailable(Character* pChar);
};

class RapidReload : public BasicTalent {
public:
	RapidReload() : BasicTalent(TALENTS::RAPID_RELOAD, APTITUDES::A_AG, APTITUDES::FIELDCRAFT, 0, "Rapid Reload") {}
	bool isAvailable(Character* pChar);
};

class Resistance : public ComplexTalent {
public:
	Resistance() : ComplexTalent(TALENTS::RESISTANCE, APTITUDES::A_T, APTITUDES::DEFENSE, 0, "Resistance") {}
	bool isAvailable(Character* pChar);
};

//FIXME take multiple times, limit of up to twice TB
class SoundConsitution : public ComplexTalent {
public:
	SoundConsitution() : ComplexTalent(TALENTS::SOUND_CONSTITUTION, APTITUDES::A_T, APTITUDES::GENERAL, 0, "Sound Consitution") {}
	bool isAvailable(Character* pChar);
};

class Takedown : public BasicTalent {
public:
	Takedown() : BasicTalent(TALENTS::TAKEDOWN, APTITUDES::A_WS, APTITUDES::OFFENSE, 0, "Takedown") {}
	bool isAvailable(Character* pChar);
};

class TechnicalKnock : public BasicTalent {
public:
	TechnicalKnock() : BasicTalent(TALENTS::TECHNICAL_KNOCK, APTITUDES::A_INT, APTITUDES::TECH, 0, "Technical Knock") {}
	bool isAvailable(Character* pChar);
};

class WarpSense : public BasicTalent {
public:
	WarpSense() : BasicTalent(TALENTS::WARP_SENSE, APTITUDES::A_PER, APTITUDES::PSYKER, 0, "Warp Sense") {}
	bool isAvailable(Character* pChar);
};

class WeaponTech : public BasicTalent {
public:
	WeaponTech() : BasicTalent(TALENTS::WEAPON_TECH, APTITUDES::A_INT, APTITUDES::TECH, 0, "Weapon-Tech") {}
	bool isAvailable(Character* pChar);
};

class WeaponTraining : public ComplexTalent {
public:

	WeaponTraining() : ComplexTalent(TALENTS::WEAPON_TRAINING, APTITUDES::GENERAL, APTITUDES::FINESSE, 0, "Weapon Training") {}
	bool isAvailable(Character* pChar);
};

//////////////////////////////////////////////////////////////////////////
// Tier Two                                                             //
//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
// Tier Three                                                           //
//////////////////////////////////////////////////////////////////////////

class AdamantiumFaith : public BasicTalent{
public:
	AdamantiumFaith() : BasicTalent(TALENTS::ADAMANTIUM_FAITH, APTITUDES::A_WP, APTITUDES::DEFENSE, 3, "Adamantium Faith") {}
};

//class X : public BasicTalent{
//public:
//	X() : BasicTalent(TALENTS::, APTITUDES::, APTITUDES::, 0, "") {}
//};
//
//class X : public ComplexTalent{
//public:
//	X() : ComplexTalent(TALENTS::, APTITUDES::, APTITUDES::, 0, "") {}
//};

//////////////////////////////////////////////////////////////////////////
// Factory                                                              //
//////////////////////////////////////////////////////////////////////////

class TalentFactory {

public:
	Talent *FactoryMethod(int type) {

		switch (type) {

			case TALENTS::AMBIDEXTROUS : {
				return new Ambidextrous;
				break;
			}
			case TALENTS::BLIND_FIGHTING : {
				return new BlindFighting;
				break;
			}
			case TALENTS::CATFALL : {
				return new Catfall;
				break;
			}
			case TALENTS::CLUES_FROM_THE_CROWDS : {
				return new CluesFromTheCrowds;
				break;
			}
			case TALENTS::DIE_HARD : {
				return new DieHard;
				break;
			}
			case TALENTS::DISARM : {
				return new Disarm;
				break;
			}
			case TALENTS::DOUBLE_TEAM : {
				return new DoubleTeam;
				break;
			}
			case TALENTS::ENEMY : {
				return new Enemy;
				break;
			}
			case TALENTS::FERRIC_SUMMONS : {
				return new FerricSummons;
				break;
			}
			case TALENTS::FRENZY : {
				return new Frenzy;
				break;
			}
			case TALENTS::IRON_JAW : {
				return new IronJaw;
				break;
			}
			case TALENTS::JADED : {
				return new Jaded;
				break;
			}
			case TALENTS::KEEN_INTUITION : {
				return new KeenIntuition;
				break;
			}
			case TALENTS::LEAP_UP : {
				return new LeapUp;
				break;
			}
			case TALENTS::NOWHERE_TO_HIDE : {
				return new NowhereToHide;
				break;
			}
			case TALENTS::PEER : {
				return new Peer;
				break;
			}
			case TALENTS::QUICK_DRAW : {
				return new QuickDraw;
				break;
			}
			case TALENTS::RAPID_RELOAD : {
				return new RapidReload;
				break;
			}
			case TALENTS::RESISTANCE : {
				return new Resistance;
				break;
			}
			case TALENTS::SOUND_CONSTITUTION : {
				return new SoundConsitution;
				break;
			}
			case TALENTS::TAKEDOWN : {
				return new Takedown;
				break;
			}
			case TALENTS::TECHNICAL_KNOCK : {
				return new TechnicalKnock;
				break;
			}
			case TALENTS::WARP_SENSE : {
				return new WarpSense;
				break;
			}
			case TALENTS::WEAPON_TECH : {
				return new WeaponTech;
				break;
			}
			case TALENTS::WEAPON_TRAINING : {
				return new WeaponTraining;
				break;
			}

			/** Tier 2 */
//			ARMOUR_MONGER,
//			BATTLE_RAGE,
//			BULGING_BICEPS,
//			COMBAT_MASTER,
//			CONSTANT_VIGILANCE,
//			CONTACT_NETWORK,
//			COORDINATED_INTERROGATION,
//			COUNTER_ATTACK,
//			COVER_UP,
//			DENY_THE_WITCH,
//			DEVISTATING_ASSULT,
//			DOUBLE_TAP,
//			EXOTIC_WEAPON_TRAINING,
//			FACE_IN_A_CROWD,
//			HARD_TARGET,
//			HARDY,
//			HATRED,
//			HIP_SHOOTING,
//			INDEPENDANT_TARGETING,
//			INESCAPABLE_ATTACK,
//			KILLING_STRIKE,
//			LUMINEN_SHOCK,
//			MAGLEV_TRANSCENDENCE,
//			MARKSMAN,
//			MECHADENDRITE_USE,
//			PRICISION_KILLER,
//			PROSANGUINE,
//			STRONG_MINDED,
//			SWIFT_ATTACK,
//			TWO_WEAPON_WIELDER,
//			UNARMED_SPECIALIST,
//			WARP_CONDUIT,
//			WHIRLWIND_OF_DEATH,

			/** Tier 3 */
			case TALENTS::ADAMANTIUM_FAITH : {
				return new AdamantiumFaith;
				break;
			}
//			ASSASIN_STRIKE,
//			BASTION_OF_IRON_WILL,
//			BLADEMASTER,
//			CRUSHING_BLOW,
//			DEATHDEALER,
//			DELICATE_INTERROGATION,
//			EYE_OF_VENGANCE,
//			FAVOURED_BY_THE_WARP,
//			FLASH_OF_INSIGHT,
//			HALO_OF_COMMAND,
//			HAMMER_BLOW,
//			INFUSED_KNOWLEDGE,
//			LIGHTNING_ATTACK,
//			LUMINEN_BLAST,
//			MASTERY,
//			MIGHTY_SHOT,
//			NEVER_DIE,
//			PRETURNATURAL_SPEED,
//			SPRINT,
//			STEP_ASIDE,
//			SUPERIOR_CHIRURGEON,
//			TARGET_SELECTION,
//			THUNDER_CHARGE,
//			TRUE_GRIT,
//			TWO_WEAPON_MASTER,
//			WARP_LOCK
			default: {
				printf("ERRORR Talent unknown!\n");
				return nullptr;
				break;
			}
		}
	}
};


class TalentChoiceNode {
private:
	std::vector<TalentChoiceNode*> mChildren;
	std::vector<TalentSelection*> mValue;

public:
	TalentChoiceNode(){};

	TalentChoiceNode(TalentSelection* pValue){
		mValue.emplace_back(pValue);
	};

	~TalentChoiceNode(){
		if (isLeaf()){
			for (TalentSelection* v : mValue){
				delete v;
			}
		} else {
			for (TalentChoiceNode* c : mChildren){
				delete c;
			}
		}
	};

	bool isLeaf() { return mChildren.empty(); }

	std::vector<TalentSelection*>* getValue() {
		if (!isLeaf()){
			return nullptr;
		} else {
			return &mValue;
		}
	}

	void addChild(TalentChoiceNode* pNode){
		mChildren.emplace_back(pNode);
	}

	std::vector<TalentChoiceNode*> getChildren() { return mChildren; }

};

/* Make a tree for choices, will generally be of form:
 * root - {A}
 *      - {B}
 * being a choice of A or B
 *
 * or
 * root - {A}
 *      - {B, C}
 *
 * being choice of {A} or {B&C}
 */
class TalentChoiceTree {
private:
	TalentChoiceNode* mRoot;


	TalentChoiceTree() : mRoot(nullptr) {}

public:

	/* Simplest and most common way of making a choice is between 2 Talents */
	TalentChoiceTree(unsigned int pTalentID1, std::string pTalentSubType1,
			unsigned int pTalentID2, std::string pTalentSubType2){
		mRoot = new TalentChoiceNode();

		TalentChoiceNode* aLeaf1 = new TalentChoiceNode(new TalentSelection(pTalentID1, pTalentSubType1));
		TalentChoiceNode* aLeaf2 = new TalentChoiceNode(new TalentSelection(pTalentID2, pTalentSubType2));

		mRoot->addChild(aLeaf1);
		mRoot->addChild(aLeaf2);
	}

	~TalentChoiceTree(){
		for (TalentChoiceNode* s : mRoot->getChildren()){
			delete s;
		}
	}

	TalentChoiceNode* getRoot() {return mRoot;}


	void makeChoices(std::vector<std::pair<unsigned int, std::string> > pRetVec){

		//FIXME never populates pRetVec

		for (TalentChoiceNode* c : mRoot->getChildren()){
			// children of root will never be leaf
			bool allLeaves = true;
			for (TalentChoiceNode* n : c->getChildren()){
				allLeaves = allLeaves & n->isLeaf();
			}

			if (allLeaves){
				// present the choice of Talents
				printf("Please choose between the following Talent sets:\n");
				int i=0;
				for (TalentChoiceNode* n : c->getChildren()){
					printf("%d:", i);
					TalentFactory* aTalentFactory = new TalentFactory();
					for (TalentSelection* s : *(n->getValue())){
						Talent* talent = aTalentFactory->FactoryMethod(s->getID());

						if (talent->isComplex()) {
							// Check if there is a set subType, otherwise ask for it
							if (s->getStringSubType().compare("")== 0) {
								printf("Gained Specialised Talent %s\n",talent->getName());
								printf("%s (Pick one)", talent->getName());
							} else {
								printf("%s (%s)", talent->getName(), s->getSubType());
							}
						} else {
							printf("%s", talent->getName());
						} //end if complex
					} // end Talent list in this choice option
					printf("\n");
					i++;
				} // end choice

			} else {
				printf("Cannot cope with mixed leaves and internals!\n");
				exit(1);
			}
		}

	}
};



#endif
