#include "Common.h"
#include "Character.h"
#include "OutputPDF.h"

#ifdef USE_BOOST_SERIAL
#include <fstream>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/access.hpp>
#endif

void printPDF(Character *pCharacter){
	OutputPDF *aOutputPDF = new OutputPDF(pCharacter);

	aOutputPDF->generate();
}

void createCharacter(Character *&pCharacter){
	HomeworldFactory *aHomeworldFactory = new HomeworldFactory();
		BackgroundFactory *aBackgroundFactory = new BackgroundFactory();
		RoleFactory *aRoleFactory = new RoleFactory();

		std::map<unsigned int, Homeworld*> sHomeworlds;
		std::map<unsigned int, Background*> sBackgrounds;
		std::map<unsigned int, Role*> sRoles;

		sHomeworlds.emplace(HOMEWORLDS::FERRAL,
				aHomeworldFactory->FactoryMethod(HOMEWORLDS::FERRAL));
		sHomeworlds.emplace(HOMEWORLDS::FORGE,
				aHomeworldFactory->FactoryMethod(HOMEWORLDS::FORGE));
		sHomeworlds.emplace(HOMEWORLDS::HIGHBORN,
				aHomeworldFactory->FactoryMethod(HOMEWORLDS::HIGHBORN));
		sHomeworlds.emplace(HOMEWORLDS::HIVE,
				aHomeworldFactory->FactoryMethod(HOMEWORLDS::HIVE));
		sHomeworlds.emplace(HOMEWORLDS::SHRINE,
				aHomeworldFactory->FactoryMethod(HOMEWORLDS::SHRINE));
		sHomeworlds.emplace(HOMEWORLDS::VOIDBORN,
				aHomeworldFactory->FactoryMethod(HOMEWORLDS::VOIDBORN));

		sBackgrounds.emplace(BACKGROUNDS::ADMIN,
				aBackgroundFactory->FactoryMethod(BACKGROUNDS::ADMIN));
		sBackgrounds.emplace(BACKGROUNDS::ARBITES,
				aBackgroundFactory->FactoryMethod(BACKGROUNDS::ARBITES));
		sBackgrounds.emplace(BACKGROUNDS::GUARD,
				aBackgroundFactory->FactoryMethod(BACKGROUNDS::GUARD));
		sBackgrounds.emplace(BACKGROUNDS::MECHANICUS,
				aBackgroundFactory->FactoryMethod(BACKGROUNDS::MECHANICUS));
		sBackgrounds.emplace(BACKGROUNDS::MINISTORUM,
				aBackgroundFactory->FactoryMethod(BACKGROUNDS::MINISTORUM));
		sBackgrounds.emplace(BACKGROUNDS::OUTCAST,
				aBackgroundFactory->FactoryMethod(BACKGROUNDS::OUTCAST));
		sBackgrounds.emplace(BACKGROUNDS::TELEPATHICA,
				aBackgroundFactory->FactoryMethod(BACKGROUNDS::TELEPATHICA));

		sRoles.emplace(ROLES::ASSASSIN,
				aRoleFactory->FactoryMethod(ROLES::ASSASSIN));
		sRoles.emplace(ROLES::CHIRURGEON,
				aRoleFactory->FactoryMethod(ROLES::CHIRURGEON));
		sRoles.emplace(ROLES::DESPERADO,
				aRoleFactory->FactoryMethod(ROLES::DESPERADO));
		sRoles.emplace(ROLES::HIEROPHANT,
				aRoleFactory->FactoryMethod(ROLES::HIEROPHANT));
		sRoles.emplace(ROLES::MYSTIC,
				aRoleFactory->FactoryMethod(ROLES::MYSTIC));
		sRoles.emplace(ROLES::SAGE,
				aRoleFactory->FactoryMethod(ROLES::SAGE));
		sRoles.emplace(ROLES::SEEKER,
				aRoleFactory->FactoryMethod(ROLES::SEEKER));
		sRoles.emplace(ROLES::WARRIOR,
				aRoleFactory->FactoryMethod(ROLES::WARRIOR));

		pCharacter = new Character(&sHomeworlds, &sBackgrounds, &sRoles);

		pCharacter->createCharacter();

		printPDF(pCharacter);
}


void spendExp(Character *pCharacter){

}

void unspendExp(Character *pCharacter){

}

void addExp(Character *pCharacter, unsigned int pNum){
	pCharacter->addExp(pNum);
}

void removeExp(Character *pCharacter, unsigned int pNum){
	pCharacter->subExp(pNum);
}

#ifdef USE_BOOST_SERIAL
void saveChar(Character *pCharacter){
	// create and open a character archive for output
	std::ofstream ofs(pCharacter->getName());

	if (ofs.is_open()){
		//FIXME add dialogue to ensure overwriting existant file

		// save data to archive
		boost::archive::text_oarchive oa(ofs);
		// write class instance to archive
		oa << *pCharacter; //segfaults!!
		// archive and stream closed when destructors are called
	} else {
		printf("Unable to open file\n");
	}
}

void loadChar(Character *&pCharacter){

	string aName = acceptString("character name");
	// create and open an archive for input
	std::ifstream ifs(aName);
	if (ifs.is_open()){
		boost::archive::text_iarchive ia(ifs);
		// read class state from archive
		ia >> pCharacter;
		// archive and stream closed when destructors are called
	} else {
		printf("Unable to open file\n");
	}
}
#endif

enum MenuOptions {
	MenuNewChar = 0,
//	MenuSpendExp,
//	MenuUnspendExp,
	MenuAddExp,
	MenuRemoveExp,
	MenuPrintPdf,
#ifdef USE_BOOST_SERIAL
	MenuSaveChar,
	MenuLoadChar,
#endif
};
int main(){

	Character *aCharacter = nullptr;

	while (true){

		printf("What would you like to do?\n");

		printf("%d: Make a new character (also outputs PDF)\n", MenuOptions::MenuNewChar);
//		printf("%d: Spend EXP\n", i);
//		printf("%d: Remove spent EXP\n", i);
		printf("%d: Add Exp\n", MenuOptions::MenuAddExp);
		printf("%d: Remove Exp)\n", MenuOptions::MenuRemoveExp);
		printf("%d: Print PDF\n", MenuOptions::MenuPrintPdf);
#ifdef USE_BOOST_SERIAL
		printf("%d: Save Character\n", MenuOptions::MenuSaveChar);
		printf("%d: Load Character\n", MenuOptions::MenuLoadChar);
#endif


		/** Make Choice */
		unsigned int aNum = acceptInt("choice");

		switch(aNum){
			case MenuOptions::MenuNewChar :{
				if (aCharacter != nullptr){
					printf("If you continue you will lose any unsaved information on your current character\n");
					printf("Are you sure you wish to continue?\n");
					printf("0: No\n;");
					printf("1: Yes\n;");

					string aSure = acceptString("if you are sure you want to continue");
					if (aSure.compare(0,1,"N") == 0 || aSure.compare(0,1,"n") == 0){
						break;
					}
				}

				createCharacter(aCharacter);
				break;
			}
			case MenuOptions::MenuPrintPdf :{
				printPDF(aCharacter);
				break;
			}
//			case MenuOptions::MenuSpendExp :{
//				//TODO
//				spendExp(aCharacter);
//				break;
//			}
//			case MenuOptions::MenuUnspendExp :{
//				//TODO
//				unspendExp(aCharacter);
//				break;
//			}
			case MenuOptions::MenuAddExp :{
				unsigned int aExp = acceptInt("");
				addExp(aCharacter, aExp);
				break;
			}
			case MenuOptions::MenuRemoveExp :{
				unsigned int aExp = acceptInt("");
				removeExp(aCharacter, aExp);
				break;
			}

#ifdef USE_BOOST_SERIAL
			case MenuOptions::MenuSaveChar :{
				//FIXME
				saveChar(aCharacter);
				break;
			}
			case MenuOptions::MenuLoadChar :{
				if (aCharacter != nullptr){
					printf("If you continue you will lose any unsaved information on your current character\n");
					printf("Are you sure you wish to continue?\n");
					printf("0: No\n;");
					printf("1: Yes\n;");

					string aSure = acceptString("if you are sure you want to continue");
					if (aSure.compare(0,1,"N") == 0 || aSure.compare(0,1,"n") == 0){
						break;
					}
				}
				loadChar(aCharacter);
				break;
			}
#endif
			default : {
				printf("Unknown option\n");
				break;
			}
		}

	}

	return 0;
}

