#ifndef DH_2E_HOMEWORLD
#define DH_2E_HOMEWORLD

#include "Common.h"

#ifdef USE_BOOST_SERIAL
// include headers that implement a archive in simple text format
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/access.hpp>
#endif

class Homeworld {

protected:

	// May be 1 or more. Impact is to add a third d10 at character generation,
	// then drop the lowest (+ve) or highest (-ve).
	std::vector<unsigned int> mPositiveCharacteristicModifiers;
	std::vector<unsigned int> mNegativeCharacteristiceModifiers;

	unsigned int mFateThreshold; // Fate threshold
	unsigned int mHighFate; 	// Chance of having 1 higher fate threshold

	std::string mBonus; 	// Home world bonus
	unsigned int mAptitude; // Home world aptitude
	unsigned int mWounds; 	// Base wounds - add 1d5

	unsigned int mWorldID;

	std::string mName;

	std::vector<unsigned int> mRecommendedBackgrounds;

#ifdef USE_BOOST_SERIAL
		friend class boost::serialization::access;
		// When the class Archive corresponds to an output archive, the
		// & operator is defined similar to <<.  Likewise, when the class Archive
		// is a type of input archive the & operator is defined similar to >>.
		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & mPositiveCharacteristicModifiers;
			ar & mNegativeCharacteristiceModifiers;

			ar & mFateThreshold; // Fate threshold
			ar & mHighFate; 	// Chance of having 1 higher fate threshold

			ar & mBonus; 	// Home world bonus
			ar & mAptitude; // Home world aptitude
			ar & mWounds; 	// Base wounds - add 1d5

			ar & mWorldID;

			ar & mName;

			ar & mRecommendedBackgrounds;
		}
#endif

public:
	Homeworld() :
			mPositiveCharacteristicModifiers(), mNegativeCharacteristiceModifiers(), mFateThreshold(
					0), mHighFate(0), mBonus(""), mAptitude(0), mWounds(0), mWorldID(
					0), mName("Unset"), mRecommendedBackgrounds() { /* EMPTY */
	}

	Homeworld(unsigned int pFateThreshold, unsigned int pHighFate,
			std::string pBonus, unsigned int pAptitude, unsigned int pWounds,
			unsigned int pWorldID, std::string pName) :
			mFateThreshold(pFateThreshold), mHighFate(pHighFate), mBonus(
					pBonus), mAptitude(pAptitude), mWounds(pWounds), mWorldID(
					pWorldID), mName(pName) {
	}

	const char* getName() {
		return mName.c_str();
	}

	std::string getStringName() {
		return mName;
	}

	unsigned int getID() {
		return mWorldID;
	}

	std::vector<unsigned int> * getPositiveCharacteristicModifiers() { return &mPositiveCharacteristicModifiers; }
	std::vector<unsigned int> * getNegativeCharacteristicModifiers() { return &mNegativeCharacteristiceModifiers; }

	unsigned int getFateThreshold(){ return mFateThreshold; }
	unsigned int getHighFate(){ return mHighFate; }
	unsigned int getWounds(){ return mWounds; }
	unsigned int getAptitude(){ return mAptitude; }

	std::string getBonus(){ return mBonus; }
	std::vector<unsigned int> getRecommendedBackgrounds(){ return mRecommendedBackgrounds; }


};

class FerralWorld: public Homeworld {

public:
	FerralWorld() :
			Homeworld(2, 3, "The Old Ways", APTITUDES::A_T, 9,
					HOMEWORLDS::FERRAL, "Ferral World") {
		mPositiveCharacteristicModifiers.emplace(
				mPositiveCharacteristicModifiers.end(), CHARACTERISTICS::S);
		mPositiveCharacteristicModifiers.emplace(
				mPositiveCharacteristicModifiers.end(), CHARACTERISTICS::T);

		mNegativeCharacteristiceModifiers.emplace(
				mNegativeCharacteristiceModifiers.end(), CHARACTERISTICS::IFL);

		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::ARBITES);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::TELEPATHICA);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::GUARD);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::OUTCAST);
	}
};

class ForgeWorld: public Homeworld {
public:
	ForgeWorld() :
			Homeworld(3, 8, "Omnissiah's Chosen", APTITUDES::A_INT, 8,
					HOMEWORLDS::FORGE, "Forge World") {
		mPositiveCharacteristicModifiers.emplace(
				mPositiveCharacteristicModifiers.end(), CHARACTERISTICS::INT);

		mPositiveCharacteristicModifiers.emplace(
				mPositiveCharacteristicModifiers.end(), CHARACTERISTICS::T);

		mNegativeCharacteristiceModifiers.emplace(
				mNegativeCharacteristiceModifiers.end(), CHARACTERISTICS::FEL);

		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::ADMIN);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::ARBITES);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::MECHANICUS);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::GUARD);
	}
};

class HighbornWorld: public Homeworld {
public:
	HighbornWorld() :
			Homeworld(4, 10, "Breeding Counts", APTITUDES::A_FEL, 9,
					HOMEWORLDS::HIGHBORN, "Highborn World") {
		mPositiveCharacteristicModifiers.emplace(
				mPositiveCharacteristicModifiers.end(), CHARACTERISTICS::FEL);
		mPositiveCharacteristicModifiers.emplace(
				mPositiveCharacteristicModifiers.end(), CHARACTERISTICS::IFL);

		mNegativeCharacteristiceModifiers.emplace(
				mNegativeCharacteristiceModifiers.end(), CHARACTERISTICS::T);

		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::ADMIN);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::ARBITES);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::TELEPATHICA);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::MINISTORUM);
	}
};

class HiveWorld: public Homeworld {
public:
	HiveWorld() :
			Homeworld(2, 6, "Teeming Masses in Metal Mountains",
					APTITUDES::A_PER, 8, HOMEWORLDS::HIVE, "Hiveworld") {
		mPositiveCharacteristicModifiers.emplace(
				mPositiveCharacteristicModifiers.end(), CHARACTERISTICS::AG);
		mPositiveCharacteristicModifiers.emplace(
				mPositiveCharacteristicModifiers.end(), CHARACTERISTICS::PER);

		mNegativeCharacteristiceModifiers.emplace(
				mNegativeCharacteristiceModifiers.end(), CHARACTERISTICS::WP);

		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::ARBITES);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::MECHANICUS);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::GUARD);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::OUTCAST);
	}
};

class ShrineWorld: public Homeworld {
public:
	ShrineWorld() :
			Homeworld(3, 6, "Faith in the Creed", APTITUDES::A_WP, 7,
					HOMEWORLDS::SHRINE, "Shrine World") {
		mPositiveCharacteristicModifiers.emplace(
				mPositiveCharacteristicModifiers.end(), CHARACTERISTICS::FEL);
		mPositiveCharacteristicModifiers.emplace(
				mPositiveCharacteristicModifiers.end(), CHARACTERISTICS::WP);

		mNegativeCharacteristiceModifiers.emplace(
				mNegativeCharacteristiceModifiers.end(), CHARACTERISTICS::PER);

		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::ADMIN);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::ARBITES);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::MINISTORUM);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::GUARD);
	}
};

class VoidbornWorld: public Homeworld {
public:
	VoidbornWorld() :
			Homeworld(3, 5, "Child of the Dark", APTITUDES::A_INT, 7,
					HOMEWORLDS::VOIDBORN, "Voidborn") {
		mPositiveCharacteristicModifiers.emplace(
				mPositiveCharacteristicModifiers.end(), CHARACTERISTICS::INT);
		mPositiveCharacteristicModifiers.emplace(
				mPositiveCharacteristicModifiers.end(), CHARACTERISTICS::WP);

		mNegativeCharacteristiceModifiers.emplace(
				mNegativeCharacteristiceModifiers.end(), CHARACTERISTICS::S);

		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::TELEPATHICA);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::MECHANICUS);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::MINISTORUM);
		mRecommendedBackgrounds.emplace(mRecommendedBackgrounds.end(),
				BACKGROUNDS::OUTCAST);
	}
};

#endif

// dont need a virtual base as will only have one factory
class HomeworldFactory {
public:
	Homeworld *FactoryMethod(int type) {
		switch (type) {
		case HOMEWORLDS::FERRAL: {
			return new FerralWorld();
			break;
		}
		case HOMEWORLDS::FORGE: {
			return new ForgeWorld();
			break;
		}
		case HOMEWORLDS::HIGHBORN: {
			return new HighbornWorld();
			break;
		}
		case HOMEWORLDS::HIVE: {
			return new HiveWorld();
			break;
		}
		case HOMEWORLDS::SHRINE: {
			return new ShrineWorld();
			break;
		}
		case HOMEWORLDS::VOIDBORN: {
			return new VoidbornWorld();
			break;
		}
		default: {
			printf("ERRORR Homeworld unknown!\n");
			return nullptr;
			break;
		}
		}
	}
};
