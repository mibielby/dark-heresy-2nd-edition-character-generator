#include "Character.h"

using std::string;

void Character::createCharacter() {

	setPlayerName();
	setCharacterName();
	setHomeworld();
	setBackground();
	setRole();

	setBackgroundFluff();

	setCharacteristics();
}

void Character::setPlayerName() {
	mPlayerName = acceptString("player's name");
}

void Character::setCharacterName() {
	mName = acceptString("Character name");
}

void Character::setHomeworld() {
	printf("Please pick your homeworld:\n");

	for (std::pair<unsigned int, Homeworld*> h : *mHomeworlds) {
		printf("%d: %s\n", h.second->getID(), h.second->getName());
	}

	unsigned int aHomeworldID = acceptInt("homeworld");

	mHomeworld = mHomeworlds->at(aHomeworldID);
	mHomeworldStr = mHomeworld->getStringName();

	unsigned int aThreshold = acceptD10("for fate threshold ");

	if (aThreshold > mHomeworld->getHighFate()) {
		mFateThreshold = mHomeworld->getFateThreshold() + 1;
	} else {
		mFateThreshold = mHomeworld->getFateThreshold();
	}

	unsigned int aWounds = acceptD5("for wounds ");

	mWounds = mHomeworld->getWounds() + aWounds;

	aptitudes.emplace(mHomeworld->getAptitude());
	mHomeworldBonus = mHomeworld->getBonus();

	printf("\n*****\n");
	printf("Your recommended backgrounds are:\n");
	for (unsigned int aBackgroundID : mHomeworld->getRecommendedBackgrounds()) {
		printf("%s\n", mBackgrounds->at(aBackgroundID)->getName());
	}
	printf("*****\n\n");
}

void Character::setBackgroundSkills() {

	/** Get skills from background */
	//This is ok as your homeworld hasn't set any of these
	std::vector<SkillSelection> startingSkills =
			mBackground->getStartingSkills();
	SkillFactory* aSkillFactory = new SkillFactory();
	for (SkillSelection skillSelection : startingSkills) {
		Skill* skill = aSkillFactory->FactoryMethod(skillSelection.getID());
		if (skill->isComplex()) {
			// Check if there is a set subType, otherwise ask for it
			if (skillSelection.getStringSubType().compare("") == 0) {
				printf("Gained Specialised Skill %s\n", skill->getName());
				string skillSpecialisation = acceptString("skill specialisation");
				static_cast<ComplexSkill*>(skill)->setSubType(
						skillSpecialisation);
			} else {
				// Sub-type supplied
				static_cast<ComplexSkill*>(skill)->setSubType(
						skillSelection.getSubType());
			}
		}
		mSkills.emplace(skill->getSkillID(), skill);
	}

#ifdef USE_CHOICE_TREE

	for (SkillChoiceTree* tree : mBackground->getStartingSkillChoices()) {
		printf("Please choose between:\n");
		unsigned int i = 0;

		std::vector < std::vector<SkillSelection*> > aSkillSelectionVecVec;

		for (SkillChoiceNode* n : tree->getRoot()->getChildren()) {
			printf("%d: ", i++); // option identifier

			std::vector<SkillSelection*> aVec;

			for (SkillSelection* s : *(n->getValue())) {
				aVec.emplace_back(s);
				Skill* skill = aSkillFactory->FactoryMethod(s->getID());
				if (skill->isComplex()) {
					printf("%s (%s)", skill->getName(),
							static_cast<ComplexSkill*>(skill)->getSubType());
				} else {
					printf("%s ", skill->getName());
				}
				printf(" "); //space between options
			}
			printf("\n"); //new line for next option
			aSkillSelectionVecVec.emplace_back(aVec);
		}

		unsigned int aIdx = acceptInt("your choice");

		// Add the chosen skills
		for (SkillSelection* s : aSkillSelectionVecVec.at(aIdx)) {
			Skill* skill = aSkillFactory->FactoryMethod(s->getID());
			if (skill->isComplex()) {
				// Check if there is a set subType, otherwise ask for it
				if (s->getStringSubType().compare("") == 0) {
					printf("Gained Specialised Skill %s\n", skill->getName());
					string skillSpecialisation = acceptString("skill specialisation");
					static_cast<ComplexSkill*>(skill)->setSubType(
							skillSpecialisation);
				} else {
					// Sub-type supplied
					static_cast<ComplexSkill*>(skill)->setSubType(
							s->getSubType());
				}
			}
			mSkills.emplace(skill->getSkillID(), skill);
		}

	}
#else
	// Pick skill from pair(s)
	//	  mBackground->getStartingSkillChoices(); TODO
#endif

}

void Character::setBackgroundTalents() {

	/** Get Talents from background */
	//This is ok as your homeworld hasn't set any of these
	std::vector<TalentSelection> startingTalents = mBackground->getStartingTalents();

	/** There may be talent choices from background */

	TalentFactory* aTalentFactory = new TalentFactory();
	for (TalentSelection TalentSelection : startingTalents) {
		Talent* talent = aTalentFactory->FactoryMethod(TalentSelection.getID());
		if (talent->isComplex()) {
			// Check if there is a set subType, otherwise ask for it
			if (TalentSelection.getStringSubType().compare("") == 0) {
				printf("Gained Specialised Talent %s\n", talent->getName());
				string TalentSpecialisation = acceptString("Talent specialisation");
				static_cast<ComplexTalent*>(talent)->setSubType(
						TalentSpecialisation);
			} else {
				// Sub-type supplied
				static_cast<ComplexTalent*>(talent)->setSubType(
						TalentSelection.getSubType());
			}
		}
		mTalents.emplace(talent->getTalentID(), talent);
	}

#ifdef USE_CHOICE_TREE
//FIXME Sub-types are empty!
	for (TalentChoiceTree* tree : mBackground->getStartingTalentChoices()) {
		printf("Please choose between:\n");
		unsigned int i = 0;

		std::vector < std::vector<TalentSelection*> > aTalentSelectionVecVec;

		for (TalentChoiceNode* n : tree->getRoot()->getChildren()) {
			printf("%d: ", i++); // option identifier

			std::vector<TalentSelection*> aVec;

			for (TalentSelection* s : *(n->getValue())) {

				aVec.emplace_back(s);
				Talent* talent = aTalentFactory->FactoryMethod(s->getID());
				if (talent->isComplex()) {
					printf("%s (%s)", talent->getName(),s->getSubType());
				} else {
					printf("%s", talent->getName());
				}
				printf(" "); //space between options
			}
			printf("\n"); //new line for next option
			aTalentSelectionVecVec.emplace_back(aVec);
		}
		unsigned int aIdx = acceptInt("your choice");

		// Add the chosen Talents
		for (TalentSelection* s : aTalentSelectionVecVec.at(aIdx)) {
			Talent* Talent = aTalentFactory->FactoryMethod(s->getID());
			if (Talent->isComplex()) {
				// Check if there is a set subType, otherwise ask for it
				if (s->getStringSubType().compare("") == 0) {
					printf("Gained Specialised Talent %s\n", Talent->getName());
					string TalentSpecialisation = acceptString("Talent specialisation");
					static_cast<ComplexTalent*>(Talent)->setSubType(
							TalentSpecialisation);
				} else {
					// Sub-type supplied
					static_cast<ComplexTalent*>(Talent)->setSubType(
							s->getSubType());
				}
			}
			mTalents.emplace(Talent->getTalentID(), Talent);
		}

	}
#else
	// Pick Talent from pair(s)
	//	  mBackground->getStartingTalentChoices(); TODO
#endif

}

void Character::setBackgroundEquipment() {

	/** Get Equipment from background */
	//This is ok as your homeworld hasn't set any of these
	equipment = mBackground->getStartingEquipment();

#ifdef USE_CHOICE_TREE

	for (EquipmentChoiceTree* tree : mBackground->getStartingEquipmentChoices()) {
		printf("Please choose between:\n");
		unsigned int i = 0;

		std::vector < std::vector<std::string> > aEquipmentelectionVecVec;

		for (EquipmentChoiceNode* n : tree->getRoot()->getChildren()) {
			printf("%d: ", i++); // option identifier

			std::vector<std::string> aVec;

			for (std::string s : *(n->getValue())) {
				aVec.emplace_back(s);
				printf("%s ", s.c_str());
			}
			printf("\n"); //new line for next option
			aEquipmentelectionVecVec.emplace_back(aVec);
		}
		unsigned int aIdx = acceptInt("your choice");

		// Add the chosen Equipment
		for (std::string s : aEquipmentelectionVecVec.at(aIdx)) {
			equipment.emplace_back(s);
		}

	}
#else
	// Pick Equipment from pair(s)
	//	  mBackground->getStartingEquipmentChoices(); TODO
#endif

}

void Character::setBackground() {
	printf("Please pick your Background:\n");

	for (std::pair<unsigned int, Background*> b : *mBackgrounds) {
		printf("%d: %s\n", b.second->getID(), b.second->getName());
	}

	unsigned int aBackgroundID = acceptInt("background");

	mBackground = mBackgrounds->at(aBackgroundID);

	mBackgroundStr = mBackground->getStringName();

	// Start grabbing info from selected background

	mBackgroundBonus = mBackground->getBackgroundBonus();


	if (mBackground->getStartingTrait().compare("") != 0) {
		mTraits.emplace_back(mBackground->getStartingTrait());
	}
	// Pick an aptitude and add it to current aptitudes

	unsigned int aAptitudeChoice = 9;
	while (aAptitudeChoice > 1) {
		printf("Please select your preferred starting aptitude:\n");
		printf("0 : ");
		printAptitude(mBackground->getBackgroundAptitudeChoices().front());
		printf("1 : ");
		printAptitude(mBackground->getBackgroundAptitudeChoices().back());
		aAptitudeChoice = acceptInt("your choice");
	}

	if (aAptitudeChoice == 0) {
		aptitudes.emplace(
				mBackground->getBackgroundAptitudeChoices().front());
	} else {
		aptitudes.emplace(
				mBackground->getBackgroundAptitudeChoices().back());
	}

	/** Get talents from background */
	setBackgroundTalents();

	/** Get skills from background */
	setBackgroundSkills();

	/** Get equipment from background */
	setBackgroundEquipment();

	printf("\n*****\n");
	printf("Your recommended roles are:\n");
	for (unsigned int aRoleID : mBackground->getRecommendedRoles()) {
		printf("%s\n", mRoles->at(aRoleID)->getName());
	}
	printf("*****\n\n");
}

void Character::setRole() {
	printf("Please pick your Role:\n");

	for (std::pair<unsigned int, Role*> r : *mRoles) {
		printf("%d: %s\n", r.second->getID(), r.second->getName());
	}

	unsigned int aIdx = acceptInt("your choice");
	mRole = mRoles->at(aIdx);
	mRoleStr = mRole->getStringName();

	// Start grabbing info from selected role

	mRoleBonus = mRole->getRoleBonus();

	for ( unsigned int a : *(mRole->getRoleAptitudesGiven())){
		aptitudes.emplace(a);
	}

	std::vector<unsigned int> aChoices = *(mRole->getRoleAptitudesChoice());
	if (!aChoices.empty()){
		printf("Please pick from the following aptitudes:\n");
		int opt=0;
		for (unsigned int i : aChoices){
			printf("%d", opt++);
			printAptitude(i);
		}
		unsigned int aIdx = acceptInt("your choice");

		aptitudes.emplace(aChoices.at(aIdx));
	}

	while (aptitudes.size() < 7) { //1 + 1 + 5
		printf("You have a repeated aptitude, pick a replacement:\n");
		unsigned int aChoiceNum=0;

		std::map<unsigned int, unsigned int> aChoiceMap;
		for (int aAptID=0; aAptID<19; aAptID++){ //aptitudes 0-18
			if (aptitudes.find(aAptID) != aptitudes.end()){
				printf("%d: ", aChoiceNum);
				printAptitude(aAptID);
				aChoiceNum++;
				aChoiceMap.emplace(aChoiceNum, aAptID);
			}
		}

		unsigned int aIdx = acceptInt("your choice");
		aptitudes.emplace(aChoiceMap[aIdx]);
	}

	printf("\n*****\n");
	printf("*****\n\n");
}

void Character::setBackgroundFluff() {
	mBackgroundFluff.mAge = acceptInt("character age");

	mBackgroundFluff.mBuild = acceptString("character build");

	mBackgroundFluff.mComplexion  = acceptString("character complexion");

	mBackgroundFluff.mDivination = acceptString("character divination");

	mBackgroundFluff.mGender = acceptString("character gender");

	mBackgroundFluff.mHair = acceptString("character hair colour");

	mBackgroundFluff.mMementos = acceptString("character mementos");

	mBackgroundFluff.mQuirks = acceptString("character quirks");

	mBackgroundFluff.mSuperstitions = acceptString("character superstitions");

	mBackgroundFluff.mNotes1 = acceptString("character notes 1");

	mBackgroundFluff.mNotes2 = acceptString("character notes 2");
}

void Character::setCharacteristics() {

	unsigned int aRolled = 0;

	string aPrompt = "Roll 2D10 ";
	string aPositivePrompt = "Roll 3D10 and take highest ";
	string aNegativePrompt = "Roll 3D10 and take lowest ";

	std::vector<string> aPrompts = {"WS", "BS", "S", "T", "AG", "INT", "PER", "WP", "FEL", "IFL"};

	for (uint i = 0; i < aPrompts.size(); i++) {

		if (mHomeworld->getPositiveCharacteristicModifiers()->front() == i
				|| mHomeworld->getPositiveCharacteristicModifiers()->back()
						== i) {
			printf("%s %s?\n", aPositivePrompt.c_str(), aPrompts[i].c_str());

		} else if (mHomeworld->getNegativeCharacteristicModifiers()->front()
				== i) {
			printf("%s %s?\n", aNegativePrompt.c_str(), aPrompts[i].c_str());

		} else {
			printf("%s %s?\n", aPrompt.c_str(), aPrompts[i].c_str());
		}

		aRolled = acceptInt("your roll");
		mCharacteristics.initCharacteristicByIndex(i, aRolled);
	}

	printf("\n#############\n");
	printf("All stats set\n");
	printf("#############\n\n");
	printAllStats();
}

void Character::printAllStats() {

	printf("+++ CHARACTER PRINTOUT +++ \n");
	printf("\n");
	printCharacteristics();
	printf("\n");
	printf("Wounds: %d\n", mWounds);
	printf("Fate threshold: %d\n", mFateThreshold);
	printf("\n");
	printf("Homeworld bonus : %s\n", mHomeworldBonus.c_str());
	printf("Background bonus: %s\n", mBackgroundBonus.c_str());
	printf("\n");
	printAptitudes();
	printf("\n");
	printSkills();
	printf("\n");
	printTalents();
	printf("\n");
	printEquipment();
	printf("\n");
	printf("+++ PRINTOUT ENDS +++ \n\n");
}

void Character::printCharacteristics() {
	printf("WS : %d\n", mCharacteristics.getWS());
	printf("BS : %d\n", mCharacteristics.getBS());
	printf("S  : %d\n", mCharacteristics.getS());
	printf("T  : %d\n", mCharacteristics.getT());
	printf("AG : %d\n", mCharacteristics.getAG());
	printf("INT: %d\n", mCharacteristics.getINT());
	printf("PER: %d\n", mCharacteristics.getPER());
	printf("WP : %d\n", mCharacteristics.getWP());
	printf("FEL: %d\n", mCharacteristics.getFEL());
	printf("IFL: %d\n", mCharacteristics.getIFL());
}

void Character::printSkills() {

	printf("== Skills ==\n");

	for (auto p : mSkills) {
		Skill* s = p.second;
		if (s->isComplex()) {
			printf("%s (%s) %s\n", s->getName(),
					static_cast<ComplexSkill*>(s)->getSubType(),
					s->getLevelString());
		} else {
			printf("%s %s\n", s->getName(), s->getLevelString());
		}
	}

	printf("============\n");
}

void Character::printTalents() {

	printf("== Talents ==\n");

//	for (auto i : talents){
//		printAptitude(i);
//	}

	printf("=============\n");
}

void Character::printAptitudes() {

	printf("== Aptitudes ==\n");

	for (auto i : aptitudes) {
		printAptitude(i);
	}

	printf("===============\n");
}

void Character::printEquipment() {

	printf("== Equipment ==\n");

	for (auto e : equipment) {
		printf("%s\n", e.c_str());
	}

	printf("===============\n");
}

