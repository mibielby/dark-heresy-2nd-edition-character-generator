#!/bin/bash

if [ $# -lt 1 ]
then
  outName="output.pdf"
else
  outName="$1.pdf"
fi

# First set the fields and their data
#nano main.py

# Then generate the data file
python main.py

# Then merge and reformat the text
pdftk master.pdf fill_form data.fdf output $outName need_appearances
