#!python

from fdfgen import forge_fdf


data = [line.strip() for line in open("input.txt", 'r')]

fields = []
for inLine in data:
    fields.append((inLine.split(',')[0],inLine.split(',')[1]))
    
#fields = [('Character Name','John Smith'),('Home World','Voidborn')]
        
fdf = forge_fdf("",fields,[],[],[])
fdf_file = open("data.fdf","wb")
fdf_file.write(fdf)
fdf_file.close()
